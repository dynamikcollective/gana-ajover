<?php include('session.php'); 
// *** Buscamos la foto del header
$sql = "SELECT nombre_campana, mecanica, descripcion, foto_nombre
        FROM info_concurso
        LIMIT 1";
$query=mysql_query($sql);
if($row=mysql_fetch_assoc($query)) $foto=$row['foto_nombre'];

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Gana Ajover | Dashboard</title>

        <!-- jQuery 2.2.3 -->
        <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
        <script src="plugins/jQueryUI/jquery-ui.min.js"></script>

        <!-- Bootstrap 3.3.6 -->
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="plugins/fastclick/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="dist/js/app.min.js"></script>
        <!-- Sparkline -->
        <script src="plugins/sparkline/jquery.sparkline.min.js"></script>
        <!-- jvectormap -->
        <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <!-- SlimScroll 1.3.0 -->
        <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- ChartJS 1.0.1 -->
        <script src="plugins/chartjs/Chart.min.js"></script>


        <script src="ate/js/md5.min.js"></script>
        <script src="ate/js/script.js"></script>

        <link rel="stylesheet" href="ate/css/style.css">

        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="dist/css/skins/skin-blue-light.css">


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition skin-blue-light sidebar-mini layout-boxed">
        <div class="wrapper">
<?php
include 'includes/main-header.php';
include 'includes/main-menu.php';
include 'conecta.php';

include("config.php");

if(isset($_GET['eid']) && isset($_GET['audit'])) {
  $eid = $_GET['eid'];
  $aud = $_GET['audit'];
  if ($aud == md5($eid.'ajbc')) {
     $res = mysql_query("delete from metas where id=$eid");
  }//endif
}//endif
?>

<section class="content-header">
    <h1>
        Administrar parametros generales de Gana Ajover<br/>
        <small>Metas del concurso</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Metas del concurso</li>
    </ol>
</section>


<section>
 
  <div class="col-md-12">
<?php
 if ($foto) {
    echo "<center><img src=\"foto/$foto\" class=\"img-responsive\"></center>";
 } else { ?>
    <img src="https://upload.wikimedia.org/wikipedia/commons/1/14/Panorama_puy_de_dome_sud.jpg" class="img-responsive">
<?php } //end if ?>
    <br>
</div>

</section>
<section class="content">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Metas del concurso</h3>
                    <p>A continuacion podr&aacute;s definir la cantidad de un producto que se debe vender por parte de cada retailer para que sus vendedores puedan acceder a los premios del programa.</p>
                </div>
                
                
   <div style="margin-left: 10%;margin-top: 5%">
	  <input type="button" value="Agregar" id="add_new"><p>
	  <table width="90%" border="0" cellpadding="0" cellspacing="0" class="table-list">
		<tr>
			<th width="60%">Producto</th>
			<th width="10%" style="text-align:center">Cantidad</th>
			<th width="20%" style="text-align:center">Unidad</th>
			<th width="10%">Acci&oacute;n</th>
		</tr>
		<?php
			$res = mysql_query("select * from metas order by producto");
			while($r = mysql_fetch_assoc($res)){
				/*
                echo '<tr>
						<td>'.$r['producto'].'</td>
						<td>'.$r['cantidad'].'</td>
						<td>'.$r['unidad'].'</td>
						<td><a href="#" id="'.$r['id'].'" class="del">Borrar</a></td>
					  </tr>';
                */
				echo '<tr>
						<td>(<font color="red">'.$r['id'].'</font>) '.$r['producto'].'</td>
						<td style="text-align:center">'.$r['cantidad'].'</td>
						<td>'.$r['unidad'].'</td>
						<td style="text-align:center"><a href="?eid='.$r['id'].'&audit='.md5($r['id'].'ajbc').'">Borrar</a></td>
					  </tr>';
			}
		?>
	  </table>
	</div>
	<div class="entry-form">
		<form name="userinfo" id="userinfo" autocomplete="off">
		<table width="90%" border="0" cellpadding="4" cellspacing="0" align="center">
			<tr>
				<td colspan="2" align="right"><a href="#" id="close">Cerrar</a></td>
			</tr>
			<tr>
				<td valign="middle" height="50px">Producto:</td>
				<td valign="middle" height="50px">
                   <select class="form-control" name="producto">
                                <?php
                                $sql="SELECT descripcion FROM productos ORDER BY descripcion ASC";
                                $query=mysql_query($sql, $connection);
                                while(list($des) = mysql_fetch_array($query)) {
                                   echo("<option value=\"$des\">$des</option>");
                                }//endwhile
                                ?>
                   </select>
                </td>
			</tr>
			<tr>
				<td valign="middle" height="50px">Cantidad:</td>
				<td valign="middle" height="50px">
                  <input type="number" name="cantidad" placeholder="Cantidad de producto" class="form-control" style="cursor: auto; background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; background-repeat: no-repeat;">
                </td>
			</tr>
			<tr>
				<td valign="middle" height="50px">Unidad:</td>
				<td valign="middle" height="50px">
                   <select class="form-control" name="unidad">
                      <option>Litros</option>
                      <option>Metros lineales</option>
                      <option>Metros cuadrados</option>
                      <option value="Metros cubicos">Metros c&uacute;bicos</option>
                      <option>Unidades</option>
                   </select>
                </td>
			</tr>
			<tr>
				<td align="right"></td>
				<td><input type="button" value="Guardar" id="save"><input type="button" value="Cancelar" id="cancel"></td>
			</tr>
		</table>
		</form><br />
	</div>

                    <div class="box-footer"> </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>

<!-- Footer -->
            </div>
            <footer class="main-footer">
                <strong>Copyright &copy; 2016 <a href="http://ajover.com">Ajover</a>.</strong> Todos los derechos reservados.
            </footer>
        </div>


    </body>
</html>
<?php
mysql_close($connection);
?>
