<?php
  include 'session.php';
  include 'includes/funciones.php';
  $idretail=$_SESSION['idretail'];
  if($_SESSION['admin']==1 || $idretail==0) {
    header('Location: dashboard-public.php');
    return;
  }//endif
  
  //*** Obtenemos los datos del Retailer
  $sql="SELECT comercial, nit, codigo, nombre_admin, cargo, email
        FROM retailers
        WHERE id=$idretail";
  $query=mysql_query($sql);
  if(!$row=mysql_fetch_assoc($query)) {
    header('Location: dashboard-public.php');
    return;
  }//endif
  $comercial = $row['comercial'];
  $nit       = $row['nit'];
  $codigo    = $row['codigo'];
  $nom       = $row['nombre_admin'];
  $cargo     = $row['cargo'];
  $email     = $row['email'];
  
  //*** Obtenemos las cantidades maximas permitidas para Retailer
  $sql = "SELECT r.idmeta, m.producto,
               r.cantidad, m.unidad
          FROM retailers_max r
               INNER JOIN metas m ON(r.idmeta=m.id)
          WHERE r.idretail=$idretail
          ORDER BY m.producto";
  $consulta = mysql_query($sql);
  $max = array();
  $i=0;
  while ($row = mysql_fetch_assoc($consulta)) {
     $max[$i]['idmeta']   = $row['idmeta'];
     $max[$i]['producto'] = $row['producto'];
     $max[$i]['cantidad'] = $row['cantidad'];
     $max[$i]['unidad']   = $row['unidad'];
     $i++;
  }//endwhile
  
  //*** Obtenemos las cantidades vendidas por Retailer
  $sql = "SELECT vd.idmeta,
               m.producto,
               m.unidad,
               SUM(vd.cantidad) AS cantidad
          FROM vendedores_detalles vd
               INNER JOIN metas m ON(vd.idmeta=m.id)
               INNER JOIN vendedores_general vg ON(vd.idvendedor=vg.id)
          WHERE vg.idretail=$idretail
          GROUP BY vd.idmeta, m.producto,m.unidad
          ORDER BY m.producto";
  $consulta = mysql_query($sql);
  $ventas = array();
  $i=0;
  while ($row = mysql_fetch_assoc($consulta)) {
     $ventas[$i]['idmeta']   = $row['idmeta'];
     $ventas[$i]['producto'] = $row['producto'];
     $ventas[$i]['cantidad'] = $row['cantidad'];
     $ventas[$i]['unidad']   = $row['unidad'];
     $i++;
  }//endwhile

  //*** Conformamos el mensaje a ser enviado a los administradores
  $mensaje = "<table width=\"400px\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
              <tr>
                <td colspan=\"2\"><div align=\"left\"><strong><u>DATOS DE RETAILER</u></strong></div></td>
              </tr>
              <tr>
                <td width=\"33%\"><div align=\"left\"><strong>C&oacute;digo:</strong></div></td>
                <td width=\"67%\"><div align=\"left\">$codigo</div></td>
              </tr>
              <tr>
                <td><div align=\"left\"><strong>Nombre Comercial:</strong></div></td>
                <td><div align=\"left\">$comercial</div></td>
              </tr>
              <tr>
                <td><div align=\"left\"><strong>Nit:</strong></div></td>
                <td><div align=\"left\">$nit</div></td>
              </tr>
              <tr>
                <td><div align=\"left\"><strong>Administrador:</strong></div></td>
                <td><div align=\"left\">$nom</div></td>
              </tr>
              <tr>
                <td><div align=\"left\"><strong>Cargo:</strong></div></td>
                <td><div align=\"left\">$cargo</div></td>
              </tr>
              <tr>
                <td><div align=\"left\"><strong>Email:</strong></div></td>
                <td><div align=\"left\">$email</div></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
            </table><br />";

  //-- Productos de promocion comprados por Retailer
  $mensaje.="<table width=\"500px\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
              <tr>
                <td colspan=\"2\"><div align=\"left\"><strong><u>PRODUCTOS DE PROMOCION ADQUIRIDOS</u></strong></div></td>
              </tr>";

  for($i=0; $i<count($max); $i++) {
    $mensaje.=" <tr>
                  <td colspan=\"2\"><div align=\"left\">".$max[$i]['producto']." <b><font color=\"green\">".
              $max[$i]['cantidad']."</font></b> ".
              $max[$i]['unidad'].
              "</div></td>
                </tr>";
  }//endfor
  $mensaje.=" <tr>
            	<td>&nbsp;</td>
            	<td>&nbsp;</td>
              </tr>
            </table>";

  //-- Productos de Promoción vendidos por Retailer
  $mensaje.="<table width=\"500px\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
              <tr>
                <td colspan=\"2\"><div align=\"left\"><strong><u>PRODUCTOS DE PROMOCION VENDIDOS</u></strong></div></td>
              </tr>";

  for($i=0; $i<count($ventas); $i++) {
    $mensaje.=" <tr>
                  <td colspan=\"2\"><div align=\"left\">".$ventas[$i]['producto']." <b><font color=\"red\">".
              $ventas[$i]['cantidad']."</font></b> ".
              $ventas[$i]['unidad'].
              "</div></td>
                </tr>";
  }//endfor
  $mensaje.=" <tr>
            	<td>&nbsp;</td>
            	<td>&nbsp;</td>
              </tr>
            </table>";
  
  //-- Premios solicitados por Vendedores
  $mensaje.="<table width=\"90%\"  border=\"1\" cellspacing=\"0\" cellpadding=\"3\" style=\"border-width: thin; border-spacing: 2px; border-style: solid; border-color: black; border-collapse: collapse;\">
              <caption>
              <strong><u>DETALLE DE PREMIOS SOLICITADOS</u></strong>
              </caption>
              <tr>
                <td><b>Vendedor</b></td>
                <td><b>Email</b></td>
                <td><b>Producto</b></td>
                <td><b>Meta</b></td>
                <td><b>Vendido</b></td>
                <td><b>Premio Solicitado</b></td>
              </tr>";
  $sql="SELECT CONCAT( vg.nombre,  ' ', vg.apellido ) AS nombre,
          vg.email, m.producto, m.cantidad, m.unidad,
          SUM( vd.cantidad ) AS vendido,
          p.descripcion AS despremio,
          vp.cantidad AS canpremio
        FROM vendedores_premios vp
          INNER JOIN metas m ON ( vp.idmeta = m.id )
          INNER JOIN vendedores_detalles vd ON ( vd.idvendedor = vp.idvendedor )
          INNER JOIN vendedores_general vg ON ( vp.idvendedor = vg.id )
          INNER JOIN premios p ON ( vp.premio = p.id )
        WHERE vg.idretail =$idretail
        GROUP BY vg.nombre, vg.apellido, vg.email, m.producto, m.cantidad, m.unidad, p.descripcion, vp.cantidad";
  $query=mysql_query($sql);
  while($row = mysql_fetch_assoc($query)) {
    $nombre   = $row['nombre'];
    $email    = $row['email'];
    $producto = $row['producto'];
    $metades  = $row['cantidad'].' '.$row['unidad'];
    $ventades = $row['vendido'].' '.$row['unidad'];
    $premio   = $row['canpremio'].' x '.$row['despremio'];
    $mensaje.="<tr>
                <td>$nombre</td>
                <td>$email</td>
                <td>$producto</td>
                <td>$metades</td>
                <td>$ventades</td>
                <td>$premio</td>
              </tr>";
  }//endwhile
  $mensaje.="</table>";
  
  /* Solo para efectos de pruebas
  enviacorreo("andyjnet@hotmail.com", $mensaje);
  die();
  */
  
  //*** Consultamos los correos de administradores para enviar mensaje
  $sql = "SELECT correo
          FROM usuarios
          WHERE admin=1";
  $query=mysql_query($sql);
  while($rs = mysql_fetch_assoc($query)) {
    enviacorreo($rs['correo'], $mensaje);
  }//end while
  
  //*** Termina Script, retorna success
  $_SESSION['prize']=1;
  header('Location: dashboard-public.php')
?>