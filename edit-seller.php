<?php
include('session.php');
include 'includes/head.php';
include 'includes/header-public.php';
include 'includes/menu-public.php';
include ("includes/funciones.php");
// *** Verificar datos de Vendedor
if(isset($_GET['eid']) && isset($_GET['hash'])) {
  $eid = $_GET['eid'];
  $aud = $_GET['hash'];
  if ($aud <> md5($eid.'ajbcrjbc')) {
    die("Ha ocurrido un error ingresando en esta p�gina...Disculpe las molestias causadas.");
  }//endif
} else {
  die("No puede llamar a esta pagina directamente");
}//endif
$idvendedor = $eid;
// *** Buscamos status de retailer
$sql = "SELECT estado FROM retailers WHERE id=$idretail";
$query = mysql_query($sql);
list($retailer_st)=mysql_fetch_array($query);

// *** Buscamos la foto
$sql = "SELECT nombre_campana, mecanica, descripcion, foto_nombre, estado
        FROM info_concurso
        LIMIT 1";
$query=mysql_query($sql);
if($row=mysql_fetch_assoc($query)) {
  $foto   = $row['foto_nombre'];
  $estado = $row['estado'];
}//endif

if(isset($_POST['actualizar']) && $_POST['actualizar'] == 1) {
 // *** Guardar Vendedor
 if (isset($_POST['nombre']) && isset($_POST['cedula'])) {
    $cedula = mysql_real_escape_string($_POST['cedula']);
    $nombre = mysql_real_escape_string($_POST['nombre']);
    $apellido = mysql_real_escape_string($_POST['apellido']);
    $premio = mysql_real_escape_string(isset($_POST['premio'])?$_POST['premio']:'');
    $email = mysql_real_escape_string($_POST['email']);

    $sql = "UPDATE vendedores_general SET
           idretail=$idretail,
           cedula='$cedula',
           nombre='$nombre',
           apellido='$apellido',
           email='$email',
           premio='$premio'
         WHERE id= $idvendedor";
    $query = mysql_query($sql);
    revisa_meta($idvendedor);
 }  //endif
 
 // *** Guardamos las unidades vendidas
 $vendido = $_POST['vendido'];
 $idmeta =  $_POST['idmeta'];
 mysql_query("DELETE FROM vendedores_detalles WHERE idvendedor=$idvendedor");
 foreach( $vendido as $key => $cantidad ) {
   if($cantidad>0 && $idmeta[$key]>0) {
      $sql = "INSERT INTO vendedores_detalles(idvendedor, idmeta, cantidad)
              VALUES($idvendedor, $idmeta[$key], $cantidad)";
      mysql_query($sql);
         }//endif
 }//end foreach
 $guardado = $_POST['actualizar'];
 
} else {
  $sql = "SELECT * FROM vendedores_general WHERE id=$idvendedor";
  $query = mysql_query($sql);
  if ($row = mysql_fetch_assoc($query)) {
    $cedula = $row['cedula'];
    $nombre = $row['nombre'];
    $apellido = $row['apellido'];
    $premio = $row['premio'];
    $email = $row['email'];
  } //end if
}//end if

?>
  <script type="text/javascript">
  function validar() {
    var error="", ruta=document.form1, str;
		//-- Validacion de cantidades
		var metas = document.getElementsByName("idmeta[]");
    var vendido = document.getElementsByName("vendido[]");
    var permitido = document.getElementsByName("permitido[]");
		var producto = document.getElementsByName("nomprod[]");
		for (var x = 0; x < metas.length; x++) {
      if(parseInt(vendido[x].value) > parseInt(permitido[x].value)){
         error += "- No puede registrar ventas mayores a las compras de "+producto[x].value+"!\n";
         break;
      }//end if
    }//end for		
		//-- Validacion de Campos
    if (ruta.nombre.value=="") error+="- Falta Nombre\n";
    if (ruta.apellido.value=="") error+="- Falta Apellido\n";
    if (ruta.cedula.value=="") error+="- Falta Cedula\n";
    if (ruta.email.value=="") error+="- Falta Email\n";
    if(error=="") {
       return true;
    } else {
       alert("Errores de Validacion:\n"+error);return false;
    }
  }
</script>
<section class="content-header">
    <h1>
    Administraci&oacute;n de vendedores<br/><br/>
        <small>
        	Ingresa la informaci&oacute;n solicitada para crear otro vendedor en tu negocio.
        </small>
    </h1>

    <ol class="breadcrumb">
        <li><a href="dashboard-public.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">A&ntilde;adir vendedores</li>
    </ol>
</section>

<br/><br/>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <h2>
            Informaci&oacute;n del vendedor
            </h2>
            <div class="row">
                 <form action="" class="form-horizontal" method="post" onsubmit="return validar();" name="form1">
                    <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label class="control-label col-xs-6" for="">Nombre:</label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="nombre" value="<?php echo $nombre ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-6" for="">Apellido:</label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="apellido" value="<?php echo $apellido ?>">
                                </div>
                            </div>
                            <!-- Campo de Premio no es necesario
                            <div class="form-group">
                                <label class="control-label col-xs-6" for="">Premio a redimir:</label>
                                <div class="col-xs-6">
                                    <select class="form-control" name="premio">
                                        <option value="">Seleccione un premio para redimir</option>
                                        <?php
                                          $sql = "SELECT id, descripcion
                                             FROM premios
                                             ORDER BY descripcion";
                                          $query = mysql_query($sql);
                                          while ($row = mysql_fetch_assoc($query)) {
                                        ?>

                                        <option value="<?php echo $row['descripcion'] ?>" <?php echo ($premio==$row['descripcion'])?"selected":"" ?>>
                                          <?php echo $row['descripcion'] ?>
                                        </option>
                                        <?php }//endwhile ?>
                                    </select>
                                </div>
                            </div>
                            -->
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label class="control-label col-xs-6" for="">Correo Electr&oacute;nico:</label>
                            <div class="col-xs-6">
                                <input type="text" class="form-control" name="email" value="<?php echo $email ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-6" for="">C&eacute;dula:</label>
                            <div class="col-xs-6">
                                <input type="text" class="form-control" name="cedula" value="<?php echo $cedula ?>">
                            </div>
                        </div>
                    </div>

            </div>
            
        </div>
        <?php if($estado==1 && $retailer_st==1) { ?>
        <div class="col-xs-12">
            <div class="row">
                <h2>
                Unidades vendidas:
                </h2>
                <div class="col-xs-12 col-sm-6">
                    <?php
										/**************
                    $sql = "SELECT m.id, m.producto, m.cantidad, m.unidad,
                              IFNULL(SUM(vd.cantidad),0) AS vendido,
															IFNULL(rm.cantidad,0) AS permitido
                         FROM metas m
                              LEFT JOIN vendedores_detalles vd ON(vd.idmeta=m.id AND vd.idvendedor=$idvendedor)
															LEFT JOIN retailers_max rm ON(rm.idretail=$idretail AND rm.idmeta=m.id)
                         GROUP BY  m.id, m.producto, m.cantidad, m.unidad
                         ORDER BY producto";
                    ***************/											 
										$sql = "SELECT m.id, m.producto, m.cantidad, m.unidad,
															(SELECT SUM(vd.cantidad)
															 FROM vendedores_detalles vd
															 WHERE vd.idmeta=m.id AND vd.idvendedor=$idvendedor
															 ) AS vendido,
															IFNULL(SUM(t.cantidad),0) AS permitido
														FROM metas m
															LEFT JOIN
																(
																	SELECT 'compra' as tipo, 
																	rm.idmeta,
																	SUM(cantidad) AS cantidad
																	FROM retailers_max rm
																	WHERE idretail = $idretail
																	GROUP BY rm.idmeta, rm.idretail
																	UNION
																	SELECT 'venta' AS tipo,
																	vd.idmeta,
																	SUM(vd.cantidad)*-1 AS cantidad
																	FROM vendedores_detalles vd
																		INNER JOIN vendedores_general vg ON(vd.idvendedor=vg.id)
																	WHERE vg.idretail = $idretail 
																	GROUP BY vd.idmeta
																) AS t ON(t.idmeta=m.id)
												GROUP BY m.id, m.producto, m.cantidad, m.unidad";										
                    $query = mysql_query($sql);
                    while ($row = mysql_fetch_assoc($query)) {
                    ?>
                    <div class="form-group">
                        <label for=""><strong>Producto:</strong>&nbsp;<?php echo $row['producto'] ?></label>
                        <div class="col-xs-6">
                            <input type="text" class="form-control" name="vendido[]" value="<?php echo $row['vendido'] ?>">
														<input type="hidden" name="nomprod[]" value="<?php echo $row['producto'] ?>">
                            <input type="hidden" name="idmeta[]" value="<?php echo $row['id'] ?>">
														<input type="hidden" name="permitido[]" value="<?php echo $row['permitido']+$row['vendido'] ?>">
                        </div>
                        <label for="" class="control-label col-xs-6"><?php echo $row['unidad'] ?></label>
                    </div>
                    <?php }//endwhile ?>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <h2>Metas a cumplir</h2>
                    <ul>
                        <?php
                        mysql_data_seek($query, 0);
                        while ($row = mysql_fetch_assoc($query)) {
                        ?>
                        <li><strong><?php echo utf8_encode($row['producto']) ?>:</strong>&nbsp;<?php echo $row['cantidad'].' '.utf8_encode($row['unidad']) ?></li>
                        <?php }//endwhile ?>
                    </ul>
                    <h2>Venta m&aacute;xima autorizada</h2>
                    <ul>
                    <?php
                    $sql = "SELECT m.producto, SUM(r.cantidad) AS cantidad, m.unidad
                         FROM retailers_max r
                              INNER JOIN metas m ON(r.idmeta=m.id)
                         WHERE r.idretail = $idretail
                         GROUP BY m.producto, m.unidad
                         ORDER BY producto";
                    $query = mysql_query($sql);
                    while ($row = mysql_fetch_assoc($query)) {
                    ?>
                        <li><strong><?php echo utf8_encode($row['producto']) ?>:</strong>&nbsp;<?php echo $row['cantidad'].' '.$row['unidad'] ?></li>
                    <?php }//endwhile
                    ?>
                    </ul>
                </div>
            </div>
        </div>
        <?php } ?>
        
    	<div class ="col-md-3 col-md-offset-6">
            <input type="hidden" name="actualizar" value="1">
            <button type="submit" class="btn btn-primary pull-right">Aceptar</button>
        </div>
     </form>
        <div class ="col-md-3">
            <button type="cancel" class="btn btn-primary" onclick="window.location.href='dashboard-public.php'">Cancelar</button>
        </div>
    </div>
</section>
<hr>
<?php
//--- Lightbox cuando el concurso esta cerrado
if($estado==0) {
	include 'includes/cerrado.php';
} elseif($retailer_st==-1) {
	include 'includes/disabled.php';
}

include 'includes/footer.php';
if(isset($guardado) && $guardado == 1) print '<script type="text/javascript">alert("Vendedor ('.$nombre.' '.$apellido.') Guardado Correctamente!");</script>';

//--- Mostrar Lightbox de concurso cerrado
if($estado==0 || $retailer_st==-1) print  "<script type=\"text/javascript\">$('#myModal').modal({backdrop: \"static\"});</script>";
?>