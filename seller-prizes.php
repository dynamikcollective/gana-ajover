<?php
include('session.php');
include 'includes/head.php';
include 'includes/header-public.php';
include 'includes/menu-public.php';
include ("includes/funciones.php");
// *** Verificar datos de Vendedor
if(isset($_GET['eid']) && isset($_GET['hash'])) {
  $eid = $_GET['eid'];
  $aud = $_GET['hash'];
  if ($aud <> md5($eid.'ajbcrjbc')) {
    die("Ha ocurrido un error ingresando en esta p�gina...Disculpe las molestias causadas.");
  }//endif
} else {
  die("No puede llamar a esta pagina directamente");
}//endif

$url = "eid=$eid&hash=$aud";
$idvendedor = $eid;

// *** Eliminamos el premio seleccionado
if(isset($_GET['epremio'])) {
  $id = $_GET['epremio'];
  $sql = "DELETE FROM vendedores_premios
          WHERE id=$id";
  mysql_query($sql);
}//end if

// *** Agregamos un registro de premio
if(isset($_GET['apremio']) && isset($_GET['idmeta']) && $_GET['apremio'] == 1) {
  $id     = $_GET['epremio'];
  $idmeta = $_GET['idmeta'];
  $sql = "INSERT INTO vendedores_premios(idvendedor, idmeta, premio, cantidad)
          VALUES($idvendedor, $idmeta, 0, 0)";
  mysql_query($sql);
}//end if

$sql="SELECT CONCAT(nombre,' ',apellido) AS nombre
      FROM vendedores_general
      WHERE id=$idvendedor";
$query=mysql_query($sql);
if (!list($nombre)=mysql_fetch_array($query))
    die("No se puede localizar el vendedor indicado (id: $idvendedor)");



if(isset($_POST['agregar']) && $_POST['agregar'] == 1) {
 // *** Guardamos los datos de Premio
 $idmeta   = $_POST['idmeta'];
 $premio   = $_POST['premio'];
 $cantidad = $_POST['cantidad'];
 
 $sql = "DELETE FROM vendedores_premios
         WHERE idvendedor=$idvendedor";
 mysql_query($sql);
 
 foreach( $idmeta as $key => $id_meta ) {
   if($id_meta > 0 &&  $premio[$key] > 0 && $cantidad[$key] > 0) {
      /* $sql = "DELETE FROM vendedores_premios
              WHERE idvendedor=$idvendedor
                AND idmeta=$id_meta
                AND premio=$premio[$key]";
       */
      $sql = "INSERT INTO vendedores_premios(idvendedor, idmeta, premio, cantidad)
              VALUES($idvendedor, $id_meta, $premio[$key], $cantidad[$key])";
      mysql_query($sql);
   }//end if
 }//end foreach
 $guardado = $_POST['agregar'];
}//endif

//*** mantenemos los premios que haya seleccionado antes para editar
$sql="SELECT idmeta, premio, cantidad
      FROM vendedores_premios
      WHERE idvendedor=$idvendedor";
$qprize = mysql_query($sql);
?>
<script type="text/javascript">
  function validar() {
    var error="", ruta=document.form1;
    var boxes = document.getElementsByName("cantidad[]");
    var cpremio = document.getElementsByName("premio[]");
    var metas = document.getElementsByName("idmeta[]");
    var max_prizes = document.getElementsByName("max_premio_meta[]");
    var producto = document.getElementsByName("producto[]");
    var maximo = 0;
    var maxp = 0;
    var meta_act = 0;
    var meta_x = 0;
    var meta_can = 0;
    var pact = "";
    for (var x = 0; x < cpremio.length; x++) {
      if(cpremio[x].value == '' || cpremio[x].value == '0'){
         error += "- Debe Seleccionar un premio!\n";
         break;
      }//end if
    }//end for

    for (var x = 0; x < boxes.length; x++) {
      if(boxes[x].value != '' && boxes[x].value != '0'){
         maximo += parseInt(boxes[x].value);
      } else {
         error += "- Debe Seleccionar una cantidad a Redimir!\n";
         break;
      }//end if
    }//end for
    
    for (var x = 0; x < metas.length; x++) {
        meta_act = parseInt(metas[x].value);
        if(meta_act != meta_x) {
          if(meta_x != 0) {
             if(meta_can > maxp) {
                error += "- No puede seleccionar mas de " + maxp + " premios de la meta (" + pact + ") para este Vendedor!";
                break;
             }//end if
          }//end if
          meta_x = meta_act;
          maxp = parseInt(max_prizes[x].value);
          meta_can = parseInt(boxes[x].value);
          pact = producto[x].value;
        } else {
          meta_can += parseInt(boxes[x].value);
        }//end if
    }//end for
    if (meta_can > maxp && error == "") {
       error += "- No puede seleccionar mas de " + maxp + " premios de la meta (" + pact + ") para este Vendedor!";
    }//end if

    if(error != "") {
      alert("Disculpe, Se encontraron errores en validacion:\n" + error);
      return false;
    }//end if
    
    if(maximo > 10) {
      alert("Disculpe, no puede seleccionar mas de diez (10) premios en total por cada Vendedor!");
      return false;
    }//endif

    return confirm("Los datos suministrados son correctos?");

  }//end function
</script>
<section class="content-header">
    <h1>
    Gestionar premio(s) de Vendedor: <?php echo $nombre ?><br/><br/>
        <small>
        	Ingresa la informaci&oacute;n solicitada para redimir premio(s) al vendedor
        </small>
    </h1>

    <ol class="breadcrumb">
        <li><a href="dashboard-public.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">configurar Premios</li>
    </ol>
</section>

<br/><br/>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <h2>
            Complete los datos a Continuaci&oacute;n:
            </h2>
            <div class="row">
            <form action="" class="form-horizontal" method="post" onsubmit="return validar();" name="form1">

              <table border="0" cellpadding="01" cellspacing="10" style="width: 90%;" class="1col-md-6 col-md-offset-1">
              <tbody>
              <tr>
                <td class ="col-md-3" valign="TOP" cellpadding="10" cellspacing="10" align="left">
                <b>Producto</b>
                </td>
                <td class ="col-md-3" valign="TOP" cellpadding="10" cellspacing="10" align="center">
                <b>Meta</b>
                </td>
                <td class ="col-md-3" valign="TOP" cellpadding="10" cellspacing="10" align="center">
                <b>Vendido</b>
                </td>
                <td class ="col-md-3" valign="TOP" cellpadding="10" cellspacing="10" align="center">
                <b>Premio a redimir</b>
                </td>
                <td class ="col-md-3" valign="TOP" cellpadding="10" cellspacing="10" align="center">
                <b>Cantidad</b>
                </td>
                <td class ="col-md-3" valign="TOP" cellpadding="10" cellspacing="10" align="center">
                <b>Acci&oacute;n</b>
                </td>
              </tr>
              <!-- bof Campos  -->
              <?php
              $sql = "SELECT  m.id, m.producto,
                        IFNULL(SUM(vd.cantidad),0) AS vendido,
                        m.cantidad AS vender,
                        m.unidad,
                        p.id AS idpremio,
                        p.descripcion,
                        vp.cantidad AS solicitado,
                        IFNULL(vp.id,0) AS idvp
                   FROM metas m
                      INNER JOIN vendedores_detalles vd ON(vd.idmeta=m.id)
                      LEFT JOIN vendedores_premios vp ON(vp.idmeta=m.id AND vp.idvendedor=vd.idvendedor)
                      LEFT JOIN premios p ON(vp.premio=p.id)
                   WHERE vd.idvendedor=$idvendedor
                   GROUP BY m.id, m.producto, m.cantidad, m.unidad,
                      p.descripcion, p.id, vp.cantidad, vp.id
                   HAVING SUM(vd.cantidad) >= m.cantidad
                   ORDER BY m.producto, vp.id, p.descripcion";
              $query2 = mysql_query($sql);
              $nmetas = 0;
              while ($row = mysql_fetch_assoc($query2)) {
                 $nmetas+=1;
                 $idmeta    = $row['id'];
                 $producto  = $row['producto'];
                 $vendido   = $row['vendido'];
                 $vender    = $row['vender'];
                 $unidad    = $row['unidad'];
                 $premios   = intval($vendido/$vender);
                 $idpremio  = $row['idpremio'];
                 $canpremio = $row['solicitado'];
                 $idvp      = $row['idvp'];
              ?>
                  <tr>
                    <td class ="col-md-3" valign="TOP" cellpadding="10" cellspacing="10" align="left" style="vertical-align: middle;">
                    <?php echo $producto ?>
                    </td>
                    <td class ="col-md-3" valign="TOP" cellpadding="10" cellspacing="10" align="center" style="vertical-align: middle;">
                    <?php echo $vender.' '.$unidad ?>
                    </td>
                    <td class ="col-md-3" valign="TOP" cellpadding="10" cellspacing="10" align="center" style="vertical-align: middle;">
                    <?php echo $vendido.' '.$unidad ?>
                    </td>

                    <td class ="col-md-3" valign="TOP" cellpadding="10" cellspacing="10" align="center" style="vertical-align: middle;">
                      <input type="hidden" name="idmeta[]" value="<?php echo $idmeta ?>">
                      <input type="hidden" name="producto[]" value="<?php echo $producto ?>">
                      <input type="hidden" name="idvp[]" value="<?php echo $idvp ?>">
                      <select name="premio[]">
                      <option hidden value="0" selected disabled>Elija un premio...</option>
                      <?php
                        $sql = "SELECT id, descripcion
                                FROM premios
                                ORDER BY descripcion";
                        $query = mysql_query($sql);
                        $pcan=0;
                      while ($row = mysql_fetch_assoc($query)) {
                        $sel="";
                        /*
                        @mysql_data_seek($qprize,0);
                        while($r = mysql_fetch_assoc($qprize)) {
                          if ($r['idmeta']==$idmeta && $r['premio']==$row['id']) {
                            $sel=" selected";
                            $pcan=$r['cantidad'];
                            break;
                          }//endif
                        }//endwhile
                        */
                        if(isset($idpremio) && $idpremio == $row['id']) {
                          $sel  = " selected";
                          $pcan = $canpremio;
                        }//end if
                        
                        echo '<option value="'.$row['id'].'"'.$sel.'>'.$row['descripcion'].'</option>';
                        
                      }//end while
                      ?>
                      </select>
                    </td>
                    
                    <td class ="col-md-3" valign="TOP" cellpadding="10" cellspacing="10" align="center" style="vertical-align: middle;">
                    <select name="cantidad[]">
                    <option hidden value="0" selected disabled></option>
                      <?php
                      //--> bof Bloquear a un maximo de 10 premios por vendedor
                      if($premios>=10) $premios=10;
                      //<-- eof
                      for($i=$premios; $i>=1; $i--) {
                        $sel="";
                        if ($i == $pcan) {
                            $sel=" selected";
                        }//endif
                        echo "<option value=\"$i\"$sel>$i</option>";
                      }//end for
                      ?>
                    </select>
                    <input type="hidden" name="max_premio_meta[]" value="<?php echo $premios ?>">
                    </td>
                    <td style="vertical-align: middle;">
                      <? if($idvp != 0 && $pcan<$premios) { ?>
                        <a href="?apremio=1&<?php echo "idmeta=$idmeta&$url&maxprizes=$premios" ?>" title="Click para Agregar premio a la meta">
                           <img src="image/award.png" title="Click para Agregar premio a la meta" />
                        </a>
                        
                        <a href="?epremio=<?php echo "$idvp&$url" ?>" title="Click para Eliminar premio de esta meta">
                           <img src="image/minus.png" title="Click para Quitar este premio de esta meta" />
                        </a>
                      <? } //end if ?>
                    </td>
                  </tr>
              <?php
              }//endwhile
              ?>
              <!-- eof Campos  -->
              <tr>
                <td class ="col-md-3" valign="TOP" cellpadding="10" cellspacing="10" align="center">
                &nbsp;
                </td>
                <td class ="col-md-3" valign="TOP" cellpadding="10" cellspacing="10" align="center">
                &nbsp;
                </td>
                <td class ="col-md-3" valign="TOP" cellpadding="10" cellspacing="10">
                &nbsp;
                </td>
                <td class ="col-md-3" valign="TOP" cellpadding="10" cellspacing="10">
                &nbsp;
                </td>
                <td class ="col-md-3" valign="TOP" cellpadding="10" cellspacing="10">
                &nbsp;
                </td>
                <td class ="col-md-3" valign="TOP" cellpadding="10" cellspacing="10">
                &nbsp;
                </td>
              </tr>

              </tbody>
              </table>

            </div>
            
        </div>

        
    	<div class ="col-md-3 col-md-offset-7">
            <input type="hidden" name="agregar" value="1">
            <button type="submit" class="btn btn-primary pull-right">Aceptar</button>
        </div>
     </form>
        <!-- <div class ="col-md-3"> -->
            <button type="cancel" class="btn btn-primary pull-left" onclick="window.location.href='dashboard-public.php'">Cancelar</button>
        <!-- </div> -->
    </div>

</section>

<hr>

<?php
include 'includes/footer.php';
if($guardado == 1) print '<script type="text/javascript">alert("Vendedor ('.$nombre.' '.$apellido.') Guardado Correctamente!");</script>';
?>