<?php
include 'session.php';
include 'includes/head.php';
include 'includes/main-header.php';
include 'includes/main-menu.php';
// *** Verificar datos de Vendedor
if(isset($_GET['eid']) && isset($_GET['hash'])) {
  $eid = $_GET['eid'];
  $aud = $_GET['hash'];
  if ($aud <> md5($eid.'ajbcrjbc')) {
    die("Ha ocurrido un error ingresando en esta p�gina...Disculpe las molestias causadas.");
  }//endif
} else {
  die("No puede llamar a esta pagina directamente");
}//endif
$idretailer = $eid;
if($_POST['actualizar'] == 1) {
 // *** Guardar Retailer
 if (isset($_POST['social']) && isset($_POST['nit'])) {
    $social = mysql_real_escape_string($_POST['social']);
    $comercial = mysql_real_escape_string($_POST['comercial']);
    $nit = mysql_real_escape_string($_POST['nit']);
    $codigo = mysql_real_escape_string($_POST['codigo']);
    $ciudad = mysql_real_escape_string($_POST['ciudad']);
    $nombre_admin = mysql_real_escape_string($_POST['nombre_admin']);
    $cargo = mysql_real_escape_string($_POST['cargo']);
    $email = mysql_real_escape_string($_POST['email']);
    $clave = mysql_real_escape_string($_POST['clave']);

    if($social <> '' && $nit <> '') {
       $sql = "UPDATE retailers SET
                 social='$social',
                 comercial='$comercial',
                 nit='$nit',
                 codigo='$codigo',
                 ciudad='$ciudad',
                 nombre_admin='$nombre_admin',
                 cargo='$cargo',
                 email='$email'
               WHERE id=$idretailer
              ";
       $query = mysql_query($sql);

       //*** Creamos el usuario
       $sql = "UPDATE usuarios SET
                 nombres='$social',
                 apellidos='$comercial',
                 login='$email',
                 clave='$clave',
                 correo='$email'
               WHERE idretail=$idretailer
              ";
       $query = mysql_query($sql);
    }//end if
 } //end if

// *** Guardamos las unidades Compradas
 $max = $_POST['metamax'];
 $idmeta =  $_POST['metaid'];
 mysql_query("DELETE FROM retailers_max WHERE idretail=$idretailer");
 foreach( $max as $key => $cantidad ) {
   if($cantidad > 0 && $idmeta[$key] > 0) {
      $sql = "INSERT INTO retailers_max(idretail, idmeta, cantidad)
              VALUES($idretailer, $idmeta[$key], $cantidad)";
      mysql_query($sql);
   }//endif
 }//end foreach
 $guardado = $_POST['actualizar'];
} else {
  $sql = "SELECT r.*, u.clave
          FROM retailers r
               LEFT JOIN usuarios u ON(u.idretail=r.id)
          WHERE r.id=$idretailer";
  $query = mysql_query($sql);
  if ($row = mysql_fetch_assoc($query)) {
    $social = $row['social'];
    $comercial = $row['comercial'];
    $nit = $row['nit'];
    $codigo = $row['codigo'];
    $ciudad = $row['ciudad'];
    $nombre_admin = $row['nombre_admin'];
    $cargo = $row['cargo'];
    $email = $row['email'];
    $clave = $row['clave'];
  } //end if
}//end if
?>

<script type="text/javascript">
  function validar() {
    var error="", ruta=document.form1, str;
    if (ruta.social.value=="") error+="- Falta Razon Social\n";
    if (ruta.comercial.value=="") error+="- Falta Nombre Comercial\n";
    if (ruta.nit.value=="") error+="- Falta NIT\n";
    if (ruta.codigo.value=="") error+="- Falta Codigo\n";
    if (ruta.nombre_admin.value=="") error+="- Falta nombre de Administrador\n";
    if (ruta.email.value=="") error+="- Falta Email\n";
    if (ruta.clave.value=="") error+="- Falta Clave de Acceso\n";
    if(error=="") {
       return true;
    } else {
       alert("Errores de Validacion:\n"+error);return false;
    }
  }
</script>
<section class="content-header">
    <h1>
        Modificar Retailer<br/><br/>
        <small>
        	Completa el siguiente formulario para agregar un retailer en la plataforma. El administrador de este retailer cargar&aacute; la informacion necesaria para completar el registro.
        </small>
    </h1>

    <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Retailers</li>
        <li class="active">Modificar retailer</li>
    </ol>
</section>

<br/><br/>
<section class="content">
    <div class="row">
    <form action="" class="" method="post" onsubmit="return validar();" name="form1">
        <div class="col-xs-12 col-sm-6">
            <h2>
            Informaci&oacute;n del comercio
            </h2>
            <div class="form-group">
                <label for="">Raz&oacute;n Social:</label>
                <input type="text" class="form-control" name="social" value="<?php echo utf8_encode($social) ?>">
            </div>
            <div class="form-group">
                <label for="">Nombre Comercial:</label>
                <input type="text" class="form-control" name="comercial" value="<?php echo utf8_encode($comercial) ?>">
            </div>
            <div class="form-group">
                <label for="">NIT:</label>
                <input type="text" class="form-control" name="nit" value="<?php echo $nit ?>">
            </div>
            <div class="form-group">
                <label for="">C&oacute;digo del cliente:</label>
                <input type="text" class="form-control" name="codigo" value="<?php echo $codigo ?>">
            </div>
            <div class="form-group">
                <label for="">Ciudad:</label>
                <input type="text" class="form-control" name="ciudad" value="<?php echo utf8_encode($ciudad) ?>">
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <h2>
            Informaci&oacute;n del administrador
            </h2>
            <div class="form-group">
                <label for="">Nombre y apellidos:</label>
                <input type="text" class="form-control" name="nombre_admin" value="<?php echo utf8_encode($nombre_admin) ?>">
            </div>
            <div class="form-group">
                <label for="">Cargo:</label>
                <input type="text" class="form-control" name="cargo" value="<?php echo utf8_encode($cargo) ?>">
            </div>
            <div class="form-group">
                <label for="">Correo electr&oacute;nico:</label>
                <input type="text" class="form-control" name="email" value="<?php echo $email ?>">
            </div>
            <div class="form-group">
                <label for="">Clave de Acceso:</label>
                <input type="text" class="form-control" name="clave" value="<?php echo utf8_encode($clave) ?>">
            </div>
        </div>
        <div class="col-xs-12">
            <h2>
            Cantidad de producto adquirido para el concurso:
            </h2>
            <div class="row">
            <?php
              $sql = "SELECT m.id, m.producto, m.cantidad, m.unidad,
                           IFNULL(SUM(r.cantidad),0) AS comprado
                      FROM metas m
                           LEFT JOIN retailers_max r ON(r.idmeta=m.id AND r.idretail=$idretailer)
                      GROUP BY m.id, m.producto, m.cantidad, m.unidad
                      ORDER BY producto";
              $query = mysql_query($sql);
              //while ($row = mysql_fetch_assoc($query)) {
            ?>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                         <?php
                           if (@mysql_data_seek($query,0)) {
                              $row = mysql_fetch_assoc($query);
                              echo '
                              <label for=""><strong>Producto:</strong>&nbsp;'.$row['producto'].'</label>
                              <div class="col-xs-6">
                              <input type="text" class="form-control" name="metamax[]" value="'.$row['comprado'].'">
                              <input type="hidden" name="metaid[]" value="'.$row['id'].'">
                              </div>
                              <label for="" class="control-label col-xs-6">'.$row['unidad'].'</label>
                               ';
                           } //end if
                         ?>
                    </div>

                    <div class="form-group">
                         <?php
                           if (@mysql_data_seek($query,1)) {
                              $row = mysql_fetch_assoc($query);
                              echo '
                              <label for=""><strong>Producto:</strong>&nbsp;'.$row['producto'].'</label>
                              <div class="col-xs-6">
                              <input type="text" class="form-control" name="metamax[]" value="'.$row['comprado'].'">
                              <input type="hidden" name="metaid[]" value="'.$row['id'].'">
                              </div>
                              <label for="" class="control-label col-xs-6">'.$row['unidad'].'</label>
                               ';
                           } //end if
                         ?>
                    </div>


                </div>

                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                         <?php
                           if (@mysql_data_seek($query,2)) {
                              $row = mysql_fetch_assoc($query);
                              echo '
                              <label for=""><strong>Producto:</strong>&nbsp;'.$row['producto'].'</label>
                              <div class="col-xs-6">
                              <input type="text" class="form-control" name="metamax[]" value="'.$row['comprado'].'">
                              <input type="hidden" name="metaid[]" value="'.$row['id'].'">
                              </div>
                              <label for="" class="control-label col-xs-6">'.$row['unidad'].'</label>
                               ';
                           } //end if
                         ?>

                    </div>
                    <div class="form-group">
                         <?php
                           if (@mysql_data_seek($query,3)) {
                              $row = mysql_fetch_assoc($query);
                              echo '
                              <label for=""><strong>Producto:</strong>&nbsp;'.$row['producto'].'</label>
                              <div class="col-xs-6">
                              <input type="text" class="form-control" name="metamax[]" value="'.$row['comprado'].'">
                              <input type="hidden" name="metaid[]" value="'.$row['id'].'">
                              </div>
                              <label for="" class="control-label col-xs-6">'.$row['unidad'].'</label>
                               ';
                           } //end if
                         ?>

                    </div>
                </div>

            <?php // }//endwhile ?>
            </div>
        </div>
        
        <div class="col-xs-12">
            <input type="hidden" name="actualizar" value="1">
            <button type="submit" class="btn btn-info pull-right">Modificar cliente</button>
        </form>
        </div>

    </div>
    
</section>

<hr>







<?php
include 'includes/footer.php';
if($guardado == 1) print '<script type="text/javascript">alert("Retailer (Nit: '.$nit.' - '.$social.') Guardado Correctamente!");</script>';
?>

