<?php
include 'session.php';
include 'includes/head.php';
include 'includes/main-header.php';
include 'includes/main-menu.php';


if(isset($_POST['agregar']) &&  $_POST['agregar'] == 1) {
 // *** Guardar Vendedor
 if (isset($_POST['social']) && isset($_POST['nit'])) {
    $social 						= mysql_real_escape_string($_POST['social']);
    $comercial 			= mysql_real_escape_string($_POST['comercial']);
    $nit 									= mysql_real_escape_string($_POST['nit']);
    $codigo 						= mysql_real_escape_string($_POST['codigo']);
    $ciudad 						= mysql_real_escape_string($_POST['ciudad']);
				$direccion				= mysql_real_escape_string($_POST['direccion']);
    $nombre_admin = mysql_real_escape_string($_POST['nombre_admin']);
    $cargo 							= mysql_real_escape_string($_POST['cargo']);
    $email 							= mysql_real_escape_string($_POST['email']);
    $clave 							= mysql_real_escape_string($_POST['clave']);
    $login 							= mysql_real_escape_string($_POST['login']);
    $txtnombre 			= mysql_real_escape_string($_POST['txtnombre']);
    $txtestado 			= mysql_real_escape_string($_POST['txtestado']);
    $txtmensaje 		= mysql_real_escape_string($_POST['txtmensaje']);
    $txtnombre2 		= mysql_real_escape_string($_POST['txtnombre2']);
    $txtestado2 		= mysql_real_escape_string($_POST['txtestado2']);
    $txtmensaje2 	= mysql_real_escape_string($_POST['txtmensaje2']);

    if($social <> '' && $nit <> '') {
       $sql = "INSERT INTO retailers(social, comercial, nit, codigo, ciudad, direccion,
																																					nombre_admin, cargo, email,
                                     txtnombre, txtestado, txtmensaje,
																																					txtnombre2, txtestado2, txtmensaje2)
               VALUES('$social','$comercial','$nit','$codigo','$ciudad','$direccion','$nombre_admin','$cargo','$email',
                      '$txtnombre','$txtestado','$txtmensaje','$txtnombre2','$txtestado2','$txtmensaje2')
              ";
       $query = mysql_query($sql);
       $idnew = mysql_insert_id();

       //*** Creamos el usuario
       $sql = "INSERT INTO usuarios(nombres, apellidos, login, clave, correo, idretail)
               VALUES('$social','$comercial','$login','$clave','$email',$idnew)
              ";
       $query = mysql_query($sql);
    }//end if
 } //end if

// *** Guardamos las unidades Compradas
 $max = $_POST['metamax'];
 $idmeta =  $_POST['metaid'];
 foreach( $max as $key => $cantidad ) {
   if($cantidad > 0 && $idmeta[$key] > 0) {
      $sql = "INSERT INTO retailers_max(idretail, idmeta, cantidad)
              VALUES($idnew, $idmeta[$key], $cantidad)";
      mysql_query($sql);
   }//endif
 }//end foreach
 $guardado = $_POST['agregar'];
}//end if
?>

<script type="text/javascript">
  function validar() {
    var error="", ruta=document.form1, str;
    if (ruta.social.value=="") error+="- Falta Razon Social\n";
    if (ruta.comercial.value=="") error+="- Falta Nombre Comercial\n";
    if (ruta.nit.value=="") error+="- Falta NIT\n";
    if (ruta.codigo.value=="") error+="- Falta Codigo\n";
    if (ruta.nombre_admin.value=="") error+="- Falta nombre de Administrador\n";
    if (ruta.email.value=="") error+="- Falta Email\n";
    if (ruta.clave.value=="") error+="- Falta Clave de Acceso\n";
    if (ruta.clave.value!=ruta.reclave.value) error+="- Las claves no coinciden\n";
    if(error=="") {
       return true;
    } else {
       alert("Errores de Validacion:\n"+error);return false;
    }
  }
</script>
<section class="content-header">
    <h1>
        Agregar Retailer<br/><br/>
        <small>
        	Completa el siguiente formulario para agregar un retailer en la plataforma. El administrador de este retailer cargar'a la informacion necesaria para completar el registro.
        </small>
    </h1>

    <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="retailers.php">Retailers</a></li>
        <li class="active">Agregar retailer</li>
    </ol>
</section>

<br/><br/>
<section class="content">
    <div class="row">
    <form action="" class="" method="post" onsubmit="return validar();" name="form1">
        <div class="col-xs-12 col-sm-6">
            <h2>
            Informaci&oacute;n del comercio
            </h2>
            <div class="form-group">
                <label for="">Raz&oacute;n Social:</label>
                <input type="text" class="form-control" name="social">
            </div>
            <div class="form-group">
                <label for="">Nombre Comercial:</label>
                <input type="text" class="form-control" name="comercial">
            </div>
            <div class="form-group">
                <label for="">NIT:</label>
                <input type="text" class="form-control" name="nit">
            </div>
            <div class="form-group">
                <label for="">C&oacute;digo del cliente:</label>
                <input type="text" class="form-control" name="codigo">
            </div>
            <div class="form-group">
                <label for="">Ciudad:</label>
                <input type="text" class="form-control" name="ciudad">
            </div>
            <div class="form-group">
                <label for="">Direcci&oacute;n:</label>
                <input type="text" class="form-control" name="direccion">
            </div>												
            <div class="form-group">
                <label for="">Nombre de la meta anual:</label>
                <input type="text" class="form-control" name="txtnombre">
            </div>
            <div class="form-group">
                <label for="">Valor de la meta anual:</label>
                <input type="text" class="form-control" name="txtestado">
            </div>
            <div class="form-group">
                <label for="">Mensaje meta anual:</label>
                <input type="text" class="form-control" name="txtmensaje">
            </div>
            <div class="form-group">
                <label for="">Nombre de la meta trimestral:</label>
                <input type="text" class="form-control" name="txtnombre2">
            </div>
            <div class="form-group">
                <label for="">Valor de la meta trimestral:</label>
                <input type="text" class="form-control" name="txtestado2">
            </div>
            <div class="form-group">
                <label for="">Mensaje meta trimestral:</label>
                <input type="text" class="form-control" name="txtmensaje2">
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <h2>
            Informaci&oacute;n del administrador
            </h2>
            <div class="form-group">
                <label for="">Nombre y apellidos:</label>
                <input type="text" class="form-control" name="nombre_admin">
            </div>
            <div class="form-group">
                <label for="">Cargo:</label>
                <input type="text" class="form-control" name="cargo">
            </div>
            <div class="form-group">
                <label for="">Correo electr&oacute;nico:</label>
                <input type="text" class="form-control" name="email">
            </div>
             
            <div class="form-group">
              <label for="">Login de Acceso:</label>
              <input type="text" class="form-control" name="login">
            </div>

            <div class="form-group">
                <label for="">Clave de Acceso:</label>
                <input type="password" class="form-control" name="clave">
            </div>
            <div class="form-group">
                <label for="">Repita Clave:</label>
                <input type="password" class="form-control" name="reclave">
            </div>
        </div>
        <div class="col-xs-12">
            <h2>
            Cantidad de producto adquirido para el concurso:
            </h2>
            <div class="row">
																<?php
																		$sql = "SELECT id, producto, cantidad, unidad
																										FROM metas
																										ORDER BY producto";
																		$query = mysql_query($sql);
																		$linea = 1;
																		//while ($row = mysql_fetch_assoc($query)) {
																?>
                <div class="col-xs-12 col-sm-6">
																		<?php
																			 while ($row = mysql_fetch_assoc($query)) {
																						 print '<div class="form-group">';
																							echo '
																								<label for=""><strong>Producto:</strong>&nbsp;'.$row['producto'].'</label>
																								<div class="col-xs-6">
																								<input type="text" class="form-control" name="metamax[]">
																								<input type="hidden" name="metaid[]" value="'.$row['id'].'">
																								</div>
																								<label for="" class="control-label col-xs-6">'.$row['unidad'].'</label>
																								';
																				} //endwhile
																				print '</div>';
																		?>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12">
            <input type="hidden" name="agregar" value="1">
            <button type="submit" class="btn btn-info pull-right">Registrar cliente</button>
        </form>
        </div>

    </div>
    
</section>

<hr>







<?php
include 'includes/footer.php';
if($guardado == 1) print '<script type="text/javascript">alert("Retailer (Nit: '.$nit.' - '.$social.') Guardado Correctamente!");</script>';
?>
