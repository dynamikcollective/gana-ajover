<?php include 'includes/head.php'; ?>
<?php include 'includes/main-header.php'; ?>
<!-- Left side column. contains the logo and sidebar -->
<?php include 'includes/main-menu.php'; ?>

<section class="content-header">
    <h1>
        Información del concurso<br/>
        <small>
        	Información general del concurso.
        </small>
    </h1>

    <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Información del concurso</li>
    </ol>
</section>

<section class="form col-md-8 col-md-offset-2">
	<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Información del conruso</h3><br/><br/>
              <p> Complete el siguiente formulario para actualizar la informacion de cada concurso de GanaAjover </p>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
              	<h4>Imagen promocional del concurso</h4>
                <div class="form-group col-xs-12">
                  <p class="help-block">Ingrese la imagen promocional del concurso para el trimestre actual.</p>
                  <input type="file" id="exampleInputFile">
                </div>
              </div>
              <!-- /.box-body -->

              <!-- Descripción y mecanica -->
              <div class="box-body">
              	<h4>Descripción y mecánica del concurso</h4>
              	<div class="form-group col-xs-12">
                  <label for="nombreCampana">Nombre de la campaña</label>
                  <input type="text" class="form-control" id="nombreCampana" placeholder="Ingresa el nombre de la campaña">
               	</div>

               	<div class="form-group col-xs-12">
                  <label>
                  	Descripción corta:
                  	<small> Ingrese descripción corta del concurso</small>
                  </label>
                  <textarea class="form-control" rows="2" placeholder=""></textarea>
              	</div>

              	<div class="form-group col-xs-12">
                  <label>
                  	Mecánica del concurso:
                  	<small> Ingrese descripción de la mecánica del concurso</small>
                  </label>
                  <textarea class="form-control" rows="5" placeholder=""></textarea>
              	</div>

              </div>	

              <!-- Descripcion corta -->
              
              
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Guardar Actualización</button>
              </div>
        	</form>
    </div>
</section>


<?php include 'includes/footer.php'; ?>



