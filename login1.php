<?php
include('login.php'); // Includes Login Script
if(isset($_SESSION['login_user'])){
//header("location: dashboard-public.php");
} //IF
include("phpcaptcha/simple-php-captcha.php");
$_SESSION['captcha'] = captcha();
$elcodigo=($_SESSION['captcha']['code']);
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GANA AJOVER | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/skins/skin-blue-light.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script type="text/javascript">
  function validar() {
    //------------------------------------------------------------------VALIDACION
    var error="", ruta=document.form1, str;
    if (ruta.username.value=="") error+="LOGIN\n";
    if (ruta.password.value=="") error+="CLAVE\n";
    if (ruta.captcha_escrito.value != "<?php print $elcodigo ?>") error+="Verifique CAPTCHA\n";
    if(error=="") {
       return true;
    } else {
       alert("Campos Requeridos:\n"+error);return false;
    }
  }
</script>
</head>
<body class="hold-transition login-page skin-blue-light">
<div class="login-box">
  <div class="login-logo">
    <a href="index.php"><img src="assets/logo-main.jpg" alt="" width="50%"></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">

<!--    <form action="index.php" method="post">___ type= email______-->
    <form method="post" action="" name="form1" id="form1" onSubmit="return validar();" autocomplete="on">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="NIT" name="username">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Contraseña" name="password" >
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      
         <div id="u23" class="ax_default image">
        <?php print '<img id="u23_image" src="' . $_SESSION['captcha']['image_src'] . '" alt="CAPTCHA" />';  ?>
      </div>
        <div id="u22" class="ax_default text_field">
        <input value="" class="form-control" placeholder="Ingrese Captcha" maxlength="5" size="14" style="" type="text" name="captcha_escrito"  id="captcha_escrito">

          <span><?php
         // php echo $error; 
          ?></span> 
      </div> 

      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Recordarme
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
      
      <!--    <button type="submit" class="btn btn-primary btn-block btn-flat">INICIAR</button>
-->
           <div style="cursor: pointer;" id="u12" class="ax_default primary_button">         
<acronym title= "Ingresar al sistema">
 <input name="submit" type="submit" value=" INICIAR " class="btn btn-primary btn-block btn-flat">
 </acronym>
</div>


           <input type="hidden" value="<?php print $elcodigo ?>" name="captcha_texto">
        </div>
        <!-- /.col -->
      </div>
    </form>

    
    <a href="#">Olvide mi contraseña</a><br>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>


</body>
</html>
