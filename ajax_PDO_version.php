<?php
error_reporting(0);
include("config.php");
if(isset($_POST) && count($_POST)){
	$producto = $_POST['producto'];
	$cantidad = $_POST['cantidad'];
	$unidad = $_POST['unidad'];
	$item_id = $_POST['item_id'];
	$action = $_POST['action'];

	if($action == "save"){
		// PDO query
		$values = array(
					':id'=>'',
					':producto'=>$producto,
					':cantidad'=>$cantidad,
					':unidad'=>$unidad
			      );

		$sql = "INSERT INTO metas VALUES (:id,:producto,:cantidad,:unidad)";
		$q = $conn->prepare($sql);
		$res = $q->execute($values);
		if($conn->lastInsertId()){
			echo json_encode(
				array(
				"success" => "1",
				"row_id" => $conn->lastInsertId(),
				"producto" => htmlentities($producto),
				"cantidad" => htmlentities($cantidad),
				"unidad" => htmlentities($unidad),
				)
			);
		}else{
			echo json_encode(array("success" => "0"));
		}
	}
	else if($action == "delete"){
		$sql = "delete from metas where id = :item_id";
		$q = $conn->prepare($sql);
		$res = $q->execute(array("item_id" => $item_id));
		
		//$res = mysql_query("delete from info where id = $item_id");
		if($res){
			echo json_encode(
				array(
				"success" => "1",
				"item_id" => $item_id
				)
			);
		}else{
			echo json_encode(array("success" => "0"));
		}
	}
}else{
	echo "No data set";
}
