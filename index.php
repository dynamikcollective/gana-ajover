<?php include('session.php'); ?>
  <?php include 'includes/head.php'; ?>
  <?php include 'includes/main-header.php'; ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include 'includes/main-menu.php'; ?>

  
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Version 2.0</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <section>
      <div class="col-md-12">
<?php
// *** Buscamos la foto del header
$sql = "SELECT nombre_campana, mecanica, descripcion, foto_nombre
        FROM info_concurso
        LIMIT 1";
$query=mysql_query($sql);
if($row=mysql_fetch_assoc($query)) $foto=$row['foto_nombre'];


 if ($foto) {
    echo "<center><img src=\"foto/$foto\" class=\"img-responsive\"></center>";
 } else { ?>
    <img src="https://upload.wikimedia.org/wikipedia/commons/1/14/Panorama_puy_de_dome_sud.jpg" class="img-responsive">
<?php } //end if ?>
    <br>
</div>
    <!--
        <div class="col-sm-12">
            <img src="https://upload.wikimedia.org/wikipedia/commons/1/14/Panorama_puy_de_dome_sud.jpg" class="img-responsive">
            <br>
        </div>
       --> 
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
       <div class="col-md-4 col-xs-12">
        <div class="small-box bg-red">
            <div class="inner">
              <h3><br/>Dashboard</h3>
            </div>
            <div class="icon">
              <i class="ion ion-home"></i>
            </div>
            <a href="info.php" class="small-box-footer bg-yellow">Más información <i class="fa fa-arrow-circle-right"></i></a>
          </div> 
       </div>
       <div class="col-md-4 col-xs-12">
        <div class="small-box bg-red">
            <div class="inner">
              <h3>Parametros<br/>de campaña</h3>

              <p></p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-cog"></i>
            </div>
            <a href="goals.php" class="col-sm-4 small-box-footer bg-yellow">Metas <i class="fa fa-arrow-circle-right"></i></a>
          
            <a href="premios.php" class="col-sm-4 small-box-footer bg-yellow">Premios <i class="fa fa-arrow-circle-right"></i></a>
          
            <a href="products.php" class="col-sm-4 small-box-footer bg-yellow">Productos <i class="fa fa-arrow-circle-right"></i></a>
          </div> 
       </div>
       <div class="col-md-4 col-xs-12">
        <div class="small-box bg-red">
            <div class="inner">
              <h3>Administración<br/>de retailers</h3>

              <p></p>
            </div>
            <div class="icon">
              <i class="ion ion-briefcase"></i>
            </div>
            <a href="retailers.php" class="small-box-footer bg-yellow">Ingresar <i class="fa fa-arrow-circle-right"></i></a>
          </div> 
       </div>
      </div>
      
      <!-- /.row -->
    </section>
    <!-- /.content -->
  <!-- /.content-wrapper -->


<!-- ./wrapper -->
<?php include 'includes/footer.php'; ?>

