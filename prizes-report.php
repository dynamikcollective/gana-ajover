<?php include('session.php'); 
// *** Script SQL de Reporte
$sql = "SELECT r.social, r.nit, r.codigo,
    r.direccion,
    v.cedula, v.nombre, v.email,
    p.descripcion AS premio,
    vp.cantidad,
    m.producto AS meta
FROM vendedores_premios vp
    INNER JOIN vendedores_general v ON ( vp.idvendedor = v.id )
    INNER JOIN premios p ON ( vp.premio = p.id )
    INNER JOIN retailers r ON ( v.idretail = r.id )
    INNER JOIN metas m ON(vp.idmeta=m.id)
ORDER BY r.social, v.nombre";
$query = mysql_query($sql);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Gana Ajover | Dashboard</title>

        <!-- jQuery 2.2.3 -->
        <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
        <script src="plugins/jQueryUI/jquery-ui.min.js"></script>

        <!-- Bootstrap 3.3.6 -->
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="plugins/fastclick/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="dist/js/app.min.js"></script>
        <!-- Sparkline -->
        <script src="plugins/sparkline/jquery.sparkline.min.js"></script>
        <!-- jvectormap -->
        <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <!-- SlimScroll 1.3.0 -->
        <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- ChartJS 1.0.1 -->
        <script src="plugins/chartjs/Chart.min.js"></script>


        <script src="ate/js/md5.min.js"></script>
        <script src="ate/js/script.js"></script>

        <link rel="stylesheet" href="ate/css/style.css">

        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="dist/css/skins/skin-blue-light.css">


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition skin-blue-light sidebar-mini layout-boxed">
        <div class="wrapper">
<?php
include 'includes/main-header.php';
include 'includes/main-menu.php';
include 'conecta.php';

include("config.php");

if(isset($_GET['eid']) && isset($_GET['audit'])) {
  $eid = $_GET['eid'];
  $aud = $_GET['audit'];
  if ($aud == md5($eid.'ajbc')) {
     $res = mysql_query("delete from metas where id=$eid");
  }//endif
}//endif
?>

<section class="content-header">
    <h1>
        Reporte de Premios solicitados por Vendedor por Retailer<br/>
        <small>Lista de Vendedores y premios solicitados</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Premios solicitados</li>
    </ol>
</section>


<section class="content">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <!-- general form elements -->
            <div class="box box-primary">
                
                
   <div style="margin-left: 2%;margin-top: 5%">
	  <p>
	  <table width="99%" border="0" cellpadding="0" cellspacing="0" class="table-list">
		<tr>
			<th>Retailer</th>
            <th style="text-align:left">NIT</th>
            <th style="text-align:left">C&oacute;digo</th>
            <th style="text-align:left">Direcci&oacute;n</th>
			<th style="text-align:left">Vendedor</th>
            <th style="text-align:left">Email</th>
            <th width="18%" style="text-align:left">Meta</th>
			<th width="18%" style="text-align:left">Premio</th>
			<th style="text-align:center">Cantidad</th>
		</tr>
		<?php
			while($r = mysql_fetch_assoc($query)){
				echo '<tr>
						<td>'.utf8_encode($r['social']).'</td>
                        <td>'.$r['nit'].'</td>
                        <td>'.$r['codigo'].'</td>
                        <td style="text-align:left">'.utf8_encode($r['direccion']).'</td>
						<td style="text-align:left">'.utf8_encode($r['nombre']).'</td>
						<td>'.$r['email'].'</td>
                        <td>'.utf8_encode($r['meta']).'</td>
						<td>'.utf8_encode($r['premio']).'</td>
            <td style="text-align:center">'.$r['cantidad'].'</td>
					  </tr>';
			}
		?>
	  </table>
	</div>
            <div class="box-footer">
                <div class ="col-md-12">
                    <a role="button" href="csv/csv_prizes-report.php" class="btn btn-primary pull-right">Descargar CSV</a>
                </div>                
            </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>

<!-- Footer -->
            </div>
            <footer class="main-footer">
                <strong>Copyright &copy; 2016 <a href="http://ajover.com">Ajover</a>.</strong> Todos los derechos reservados.
            </footer>
        </div>


    </body>
</html>
<?php
mysql_close($connection);
?>