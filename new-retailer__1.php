<?php include 'includes/head.php'; ?>
<?php include 'includes/main-header.php'; ?>
<!-- Left side column. contains the logo and sidebar -->
<?php include 'includes/main-menu.php'; ?>

<section class="content-header">
    <h1>
        Agregar Retailer<br/><br/>
        <small>
        	Completa el siguiente formulario para agregar un retailer en la plataforma. El administrador de este retailer cargar'a la informacion necesaria para completar el registro.
        </small>
    </h1>

    <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Retailers</li>
        <li class="active">Agregar retailer</li>
    </ol>
</section>

<br/><br/>
<section class="content">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h2>
            Información del comercio
            </h2>
            <div class="form-group">
                <label for="">Razón Social:</label>
                <input type="text" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Nombre Comercial:</label>
                <input type="text" class="form-control">
            </div>
            <div class="form-group">
                <label for="">NIT:</label>
                <input type="text" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Código del cliente:</label>
                <input type="text" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Ciudad:</label>
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <h2>
            Información del administrador
            </h2>
            <div class="form-group">
                <label for="">Nombre y apellidos:</label>
                <input type="text" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Cargo:</label>
                <input type="text" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Correo electrónico:</label>
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-xs-12">
            <h2>
            Cantidad de producto adquirido para el concurso:
            </h2>
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for=""><strong>Producto:</strong> TORNILLERIA PARA CUBIERTAS AJOVER</label>
                        <div class="col-xs-6">
                            <input type="text" class="form-control">
                        </div>
                        <label for="" class="control-label col-xs-6">Unidades</label>
                    </div>
                    <div class="form-group">
                        <label for=""><strong>Producto:</strong> TORNILLERIA PARA CUBIERTAS AJOVER</label>
                        <div class="col-xs-6">
                            <input type="text" class="form-control">
                        </div>
                        <label for="" class="control-label col-xs-6">Unidades</label>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for=""><strong>Producto:</strong> TORNILLERIA PARA CUBIERTAS AJOVER</label>
                        <div class="col-xs-6">
                            <input type="text" class="form-control">
                        </div>
                        <label for="" class="control-label col-xs-6">Unidades</label>
                    </div>
                    <div class="form-group">
                        <label for=""><strong>Producto:</strong> TORNILLERIA PARA CUBIERTAS AJOVER</label>
                        <div class="col-xs-6">
                            <input type="text" class="form-control">
                        </div>
                        <label for="" class="control-label col-xs-6">Unidades</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <button type="submit" class="btn btn-info pull-right">Registrar cliente</button>
    
        </div>
    </div>
    
</section>

<hr>







<?php include 'includes/footer.php'; ?>

