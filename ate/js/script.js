$(document).ready(function(){
	
	$("#save").click(function(){
		ajax("save");
	});

    $("#savep").click(function(){
		ajax("savep");
	});

    $("#add_new").click(function(){
		$(".entry-form").fadeIn("fast");	
	});
	
	$("#close").click(function(){
		$(".entry-form").fadeOut("fast");	
	});
	
	$("#cancel").click(function(){
		$(".entry-form").fadeOut("fast");	
	});
	
	$(".del").live("click",function(){
		if(confirm("Seguro que desea eliminar este registro ?")){
			ajax("delete",$(this).attr("id"));
		}
	});

	function ajax(action,id){
		if(action =="save")
			data = $("#userinfo").serialize()+"&action="+action;
        else if(action=="savep")
            data = $("#userinfo").serialize()+"&action="+action;
		else if(action == "delete"){
			data = "action="+action+"&item_id="+id;
		}
		

		$.ajax({
			type: "POST", 
			url: "ajax.php", 
			data : data,
			dataType: "json",
			success: function(response){
				if(response.success == "1"){
					if(action == "save"){
						$(".entry-form").fadeOut("fast",function(){
							$(".table-list").append("<tr><td>"+response.producto+"</td><td>"+response.cantidad+"</td><td>"+response.unidad+"</td><td><a href='?eid="+response.row_id+"&audit="+md5(response.row_id+'ajbc')+"' id='"+response.row_id+"' class='del'>Borrar</a></a></td></tr>");
							$(".table-list tr:last").effect("highlight", {
								color: '#4BADF5'
							}, 1000);
						});	
						$(".entry-form input[type='text']").each(function(){
							$(this).val("");
						});		
					}else if(action == "delete"){
						var row_id = response.item_id;
						$("a[id='"+row_id+"']").closest("tr").effect("highlight", {
							color: '#4BADF5'
						}, 1000);
						$("a[id='"+row_id+"']").closest("tr").fadeOut();
					}else if(action == "savep"){
						$(".entry-form").fadeOut("fast",function(){
							$(".table-list").append("<tr><td>"+response.descripcion+"</td><td><a href='?eid="+response.row_id+"&audit="+md5(response.row_id+'ajbc')+"' id='"+response.row_id+"' class='del'>Borrar</a></a></td></tr>");
							$(".table-list tr:last").effect("highlight", {
								color: '#4BADF5'
							}, 1000);
						});
						$(".entry-form input[type='text']").each(function(){
							$(this).val("");
						});
					}
				}else{
					alert("unexpected error occured, Please check your database connection");
				}
			},
			error: function(res){
				alert("Unexpected error! Try again.");
			}
		});
	}
});
