<?php
include('session.php');
include 'includes/head.php';
include 'includes/main-header.php';
include 'includes/main-menu.php';

// *** Eliminar Registro
if(isset($_GET['eid']) && isset($_GET['hash'])) {
  $eid = $_GET['eid'];
  $aud = $_GET['hash'];
  if ($aud == md5($eid.'ajbcrjbc')) {
     $res = mysql_query("delete from retailers where id=$eid");
     $res = mysql_query("delete from usuarios where idretail=$eid");
  }//endif
}//endif

// *** Buscar datos para la tabla de Retailers
$sql = "SELECT r.id, r.social, r.nombre_admin,
          COUNT(vg.id) AS vendedores
     FROM retailers r
          LEFT JOIN vendedores_general vg ON(vg.idretail=r.id)
     GROUP BY r.id, r.social, r.nombre_admin
     ORDER BY r.social";
$query = mysql_query($sql);
$linea = 1;
?>
<style> 
div#commentFormd{margin: 0px 20px 0px 20px;display: none;}
div#commentFormd2{margin: 0px 20px 0px 20px;display: none;}
</style> 

<script type="text/javascript">
  function eliminar(social, eid, hash) {
    var resp = confirm("Confirme que desea eliminar este Retail?");

    if (resp == true) {
       window.location.href='?eid='+eid+'&hash='+hash;
    }
  }
</script>


<script type="text/javascript"> 
function toggleLayer(whichLayer)
  {
  if (document.getElementById)
  {
  // this is the way the standards work
  var style2 = document.getElementById(whichLayer).style;
  style2.display = style2.display? "":"block";
  }
  else if (document.all)
  {
  // this is the way old msie versions work
  var style2 = document.all[whichLayer].style;
  style2.display = style2.display? "":"block";
  }
  else if (document.layers)
  {
  // this is the way nn4 works
  var style2 = document.layers[whichLayer].style;
  style2.display = style2.display? "":"block";
}
}
</script> 




<section class="content-header">
    <h1>
        Administrar Retailers<br/><br/>
        <small>
        	A continuaci&oacute;n, se presentan los reatailers creados en la plataforma. Cada uno es un administrados del sistema que le permite cargar sus vendedores con su estado de puntos.
        </small>
    </h1>

    <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Retailers</li>
    </ol>
</section>

<br/><br/>
<section class="retailers-table">
	 <div class="row" id="row">
		<div class ="col-md-11">
				<a role="button" href="csv/csv_retailers.php" class="btn btn-default pull-right">Descargar listado CSV</a>
		</div>		
	 </div>
	<div class="col-sm-12"><table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                <thead>
                <tr role="row">
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending">ID</th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending">Raz&oacute;n Social</th>
                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column descending" aria-sort="ascending">Nombre administrador</th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending"># Vend.</th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Meta 1</th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">Meta 2</th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Meta 3</th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">Meta 4</th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="text-align:center">Acciones</th></tr>
                </thead>
                <tbody>
                
                <?php while ($row = mysql_fetch_assoc($query)) {
                   $eid = $row['id'];
                   $hash =  md5($row['id'].'ajbcrjbc');
                ?>
                <tr role="row" class="<?php echo (($linea%2)==0)?'even':'odd' ?>">
                  <td class=""><?php echo "<font color='red'>$eid</font>" ?></td>
                  <td class=""><?php echo utf8_encode($row['social']) ?></td>
                  <td class="sorting_1"><?php echo substr(utf8_encode($row['nombre_admin']),0,25).((strlen($row['nombre_admin'])>25)?'...':'') ?></td>
                  <td class="text-center"><?php echo $row['vendedores'] ?></td>
                   <?php
                    $sql = "SELECT m.id, m.cantidad AS meta,
                                SUM(vd.cantidad) AS vendido
                            FROM retailers r
                            	INNER JOIN vendedores_general vg ON(vg.idretail=r.id)
                             	INNER JOIN vendedores_detalles vd ON(vd.idvendedor=vg.id)
                              	INNER JOIN metas m ON(vd.idmeta=m.id)
                            WHERE vg.idretail =  {$row['id']}
                            GROUP BY m.id, m.producto, m.cantidad
                            ORDER BY m.producto";
                    $query2 = mysql_query($sql);
                    $nmetas = 0;
                    while ($row = mysql_fetch_assoc($query2)) {
                       $nmetas+=1;
                       $vendido = $row['vendido'];
                       $meta = $row['meta'];
                       $meta_por = $vendido*100/$meta;
											 $meta_por = number_format($meta_por, 2);
                    ?>
                       <td class="text-center"><?php echo "$meta_por%" ?></td>
                    <?php
                    } //endwhile
                    for($i=$nmetas; $i<4; $i++) {
                      echo '<td class="text-center">&nbsp;</td>';
                    }//end for
                    ?>
                  <td>
                  	<div class="row">
                  		<div class ="col-md-6">
                			<button type="submit" class="btn btn-xs btn-primary pull-right" onclick="window.location.href='edit-retailer.php?eid=<?php echo $eid."&hash=$hash" ?>'">Editar</button>
              			</div>
              			<div class ="col-md-6">
              			    <button type="submit" class="btn btn-xs btn-primary pull-right"  onclick="eliminar(<?php echo "'{$row['social']}','$eid','$hash'" ?>);">Eliminar</button>
              			</div>
                  	</div>
                  </td>
                </tr>
                <?php
                $linea+=1;
                } //end while
                ?>

                
            	</tbody>
            </table>
    </div>

    <div id="row" class="row">
    	<div class ="col-md-2 col-md-offset-1">
            <button type="submit" class="btn btn-primary pull-left">Reset de ventas y Puntos</button>
 <!--
<br><br>
 <a  class="btn btn-primary"  href="javascript:toggleLayer('commentFormd2');" title=" "  onclick =  ""  >
Masivo Adquirido por Retailers
</a>

<div id="commentFormd2" class =""><fieldset>
<form action="csv/upload_rmax.php" method="post" enctype="multipart/form-data">
<input type="file" name="csv" value="" />
<input type="submit" name="submit" value="Subir CSV" /> 
<input type="reset" name="" value="Cancelar">
</form></fieldset>

</div>
-->
      </div>
        
    	<div class ="col-md-3">
            <a role="button" href="new-retailer.php" class="btn btn-primary pull-right">Agregar nuevo retailer</a>
      </div>

      <div class ="col-md-3">
        <!-- <button type="submit" class="btn btn-primary">Nuevos retailers desde CSV</button> -->
        <a class="btn btn-primary"  href="javascript:toggleLayer('commentFormd');" title="password;username;razon_social;nombre_comercial;nit;codigo_de_cliente;ciudad;nombre_admin;cargo_admin;correo_admin;" onclick ="">
          Nuevos retailers desde CSV
        </a>
        <!-- <br /><br /> -->
        <div id="commentFormd" class ="">
          <form action="csv/upload_r.php" method="post" enctype="multipart/form-data">
            <input type="file" name="csv" value="" />
            <input type="submit" name="submit" value="Subir CSV" />
          </form>
        </div>
      </div>

      <div class ="col-md-3">
        <a role="button" class="btn btn-primary pull-left" href="javascript:toggleLayer('commentFormd2');" title=" "  onclick =  ""  >
          Compras Retailers desde CSV
        </a>
        <br /><br />
        <div id="commentFormd2" class =""><fieldset>
        <form action="csv/upload_rmax.php" method="post" enctype="multipart/form-data">
          <input type="file" name="csv" value="" />
          <input type="submit" name="submit" value="Subir CSV" />
          <input type="reset" name="" value="Cancelar">
        </form></fieldset>
      </div>

    </div>
</section>

<hr />


<?php include 'includes/footer.php'; ?>
