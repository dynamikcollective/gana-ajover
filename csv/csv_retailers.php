<?php
include("../session.php");
ob_clean();
header("Content-Type: text/csv");
header("Content-Transfer-Encoding: UTF-8");
header("Content-Disposition: attachment; filename=report_retailers.csv");
header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
header("Pragma: no-cache"); // HTTP 1.0
header("Expires: 0"); // Proxies
echo "\xEF\xBB\xBF"; // UTF-8 BOM

$sql = "SELECT r.id, r.nit, r.codigo, r.social, r.direccion
        FROM retailers r 
        ORDER BY r.id";
$query = mysql_query($sql);

$output = fopen("php://output", "w");
fputcsv($output, array('ID', 'NIT', 'Codigo','Razon Social','Direccion'), ";", '"');
while ($row = mysql_fetch_assoc($query)) fputcsv($output, $row, ";", '"');
fclose($output);
?>