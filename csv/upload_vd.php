<?php
include("../session.php");
include ("../includes/funciones.php");
$csv 	   = array();
$permitido = array();
if($_FILES['csv']['error'] == 0) {
    $name = $_FILES['csv']['name'];
	$ext = explode('.', $_FILES['csv']['name']);
	$ext = end($ext);
    $ext = strtolower($ext);
    $type = $_FILES['csv']['type'];
    $tmpName = $_FILES['csv']['tmp_name'];

    // check the file is a csv
    if($ext === 'csv'){
        if(($handle = fopen($tmpName, 'r')) !== FALSE) {
			//-- Obtenemos los valores maximos permitidos segun las compras vs Ventas del Retailer
			$sql = "SELECT t.idmeta, 
						SUM(t.cantidad) AS cantidad
					FROM (
						SELECT 'compra' as tipo, 
						rm.idmeta,
						SUM(cantidad) AS cantidad
						FROM retailers_max rm
						WHERE idretail = $idretail
						GROUP BY rm.idmeta, rm.idretail
						UNION
						SELECT 'venta' AS tipo,
						vd.idmeta,
						SUM(vd.cantidad)*-1 AS cantidad
						FROM vendedores_detalles vd
						  INNER JOIN vendedores_general vg ON(vd.idvendedor=vg.id)
						WHERE vg.idretail = $idretail 
						GROUP BY vd.idmeta
					) AS t
					GROUP BY t.idmeta";
			$query = mysql_query($sql,$connection);
			$i = 0;
			while($row = mysql_fetch_assoc($query)) {
				$permitido[$i]['idmeta']  	= $row['idmeta'];
				$permitido[$i]['cantidad']	= $row['cantidad'];
				$i++;
			}
			
            // necessary if a large csv file
            set_time_limit(0);
            $row = 0;
            while(($data = fgetcsv($handle, 1000, ';')) !== FALSE) {
                // number of fields in the csv
                $col_count = count($data);
                // get the values from the csv 
                $csv[$row]['col1'] = $data[0];
                $csv[$row]['col2'] = $data[1];
                $csv[$row]['col3'] = $data[2];

                $row++;
            }//end while
            fclose($handle);
            
            // bof Manipulamos datos
            if($row>0) {

               for($i=1; $i < $row; $i++) { 
                  $idvendedor=utf8_encode(mysql_real_escape_string($csv[$i]['col1']));
                  $idmeta=utf8_encode(mysql_real_escape_string($csv[$i]['col2']));
                  $cantidad=utf8_encode(mysql_real_escape_string($csv[$i]['col3']));
				  
				  //-- Validamos las cantidades maximas permitidas
				  $sql   = "SELECT IFNULL(sum(cantidad),0) AS vendido FROM vendedores_detalles WHERE idvendedor=$idvendedor AND idmeta=$idmeta";
				  $vendido = 0;
				  if($query = mysql_query($sql)) list($vendido)=mysql_fetch_array($query);
				  
				  $sw = true;
				  for($x=0; $x<count($permitido); $x++) {
					if(intval($idmeta) == intval($permitido[$x]['idmeta']) && intval($cantidad) > (intval($permitido[$x]['cantidad']) + $vendido)) {
						$sw = false;
						break;
					}
				  }
				  
				  if($sw) {
					//*** Primero actualizamos el registro
					$sql="UPDATE vendedores_detalles SET
							cantidad='$cantidad'
						  WHERE idvendedor=$idvendedor
							AND idmeta=$idmeta";
					$query = @mysql_query($sql,$connection);
					
					//*** Luego ejecutamos Insert Ignore para evitar duplicados
					$sql="INSERT IGNORE INTO vendedores_detalles(idvendedor,idmeta,cantidad)
						  VALUES('$idvendedor','$idmeta','$cantidad');
						  ";
					$query = @mysql_query($sql,$connection);
					revisa_meta($idvendedor);
				  } //end if
               }//end for
            }//end if
            header("Location: ../dashboard-public.php");
        }//end if
    }//end if
    
}  else {
  //*** Error leyendo csv
	  header("Location: ../dashboard-public.php");
}//end if
?>