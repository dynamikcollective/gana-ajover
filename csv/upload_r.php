<?php
include("../session.php");
$csv = array();
if($_FILES['csv']['error'] == 0){
    $name = $_FILES['csv']['name'];
    $ext = strtolower(end(explode('.', $_FILES['csv']['name'])));
    $type = $_FILES['csv']['type'];
    $tmpName = $_FILES['csv']['tmp_name'];

    // check the file is a csv
    if($ext === 'csv') {
        if(($handle = fopen($tmpName, 'r')) !== FALSE) {
            // necessary if a large csv file
            set_time_limit(0);
            $row = 0;
            while(($data = fgetcsv($handle, 1000, ';')) !== FALSE) {
                // number of fields in the csv
                $col_count = count($data);
                // get the values from the csv
              $csv[$row]['col1'] = $data[0];
              $csv[$row]['col2'] = $data[1];
              $csv[$row]['col3'] = $data[2];
              $csv[$row]['col4'] = $data[3];
              $csv[$row]['col5'] = $data[4];
              $csv[$row]['col6'] = $data[5];
              $csv[$row]['col7'] = $data[6];
              $csv[$row]['col8'] = $data[7];
              $csv[$row]['col9'] = $data[8];
              $csv[$row]['col10'] = $data[9];
              $csv[$row]['col11'] = $data[10];
              $csv[$row]['col12'] = $data[11];
              $csv[$row]['col13'] = $data[12];
              $csv[$row]['col14'] = $data[13];
              $csv[$row]['col15'] = $data[14];
              $csv[$row]['col16'] = $data[15];
              $csv[$row]['col17'] = $data[16];
              $csv[$row]['col18'] = $data[17];
                // inc the row
                $row++;
            }//end while
            fclose($handle);
            
            // bof Manipulamos datos
            if($row) {
               for($i=1; $i < $row; $i++) {

                $clave        = utf8_encode(mysql_real_escape_string($csv[$i]['col1']));
                $username     = utf8_encode(mysql_real_escape_string($csv[$i]['col2']));
                $social       = utf8_encode(mysql_real_escape_string($csv[$i]['col3']));
                $comercial    = utf8_encode(mysql_real_escape_string($csv[$i]['col4']));
                $nit          = utf8_encode(mysql_real_escape_string($csv[$i]['col5']));
                $codigo       = utf8_encode(mysql_real_escape_string($csv[$i]['col6']));
                $ciudad       = utf8_encode(mysql_real_escape_string($csv[$i]['col7']));
                $direccion    = utf8_encode(mysql_real_escape_string($csv[$i]['col8']));
                $nombre_admin = utf8_encode(mysql_real_escape_string($csv[$i]['col9']));
                $cargo        = utf8_encode(mysql_real_escape_string($csv[$i]['col10']));
                $email        = utf8_encode(mysql_real_escape_string($csv[$i]['col11']));
                $txtnombre    = utf8_encode(mysql_real_escape_string($csv[$i]['col12']));
                $txtestado    = utf8_encode(mysql_real_escape_string($csv[$i]['col13']));
                $txtmensaje   = utf8_encode(mysql_real_escape_string($csv[$i]['col14']));
                $txtnombre2   = utf8_encode(mysql_real_escape_string($csv[$i]['col15']));
                $txtestado2   = utf8_encode(mysql_real_escape_string($csv[$i]['col16']));
                $txtmensaje2  = utf8_encode(mysql_real_escape_string($csv[$i]['col17']));
                $estado       = @intval($csv[$i]['col18']);
                $estado       = ($estado == 0)?1:$estado;
                /*
                $sql="INSERT IGNORE INTO retailers(cargo,ciudad,codigo,comercial,email,nit,nombre_admin,social) 
                                VALUES('$cargo','$ciudad','$codigo','$comercial','$email','$nit','$nombre_admin','$social');";
                                $query=mysql_query($sql,$connection);
                */
                 
                $sql="UPDATE retailers SET
                        cargo='$cargo',ciudad='$ciudad',codigo='$codigo',
                        comercial='$comercial',email='$email',
                        nombre_admin='$nombre_admin',
                        social='$social',
                        estado=$estado,
                        direccion='$direccion',
                        txtnombre='$txtnombre'
                        ,txtestado='$txtestado'
                        ,txtmensaje='$txtmensaje'
                        ,txtnombre2='$txtnombre2'
                        ,txtestado2='$txtestado2'
                        ,txtmensaje2='$txtmensaje2'
                      WHERE nit=$nit";
                $query=mysql_query($sql,$connection);
                $sql="INSERT IGNORE INTO retailers(cargo,ciudad,codigo,comercial,email,nit,nombre_admin,social,
                                                   txtnombre,txtestado,txtmensaje,txtnombre2,txtestado2,txtmensaje2,estado,direccion)
                                VALUES('$cargo','$ciudad','$codigo','$comercial','$email','$nit','$nombre_admin','$social',
                                       '$txtnombre','$txtestado','$txtmensaje',
                                       '$txtnombre2','$txtestado2','$txtmensaje2',$estado,'$direccion');";
                $query=mysql_query($sql,$connection);
                $idretailx=mysql_insert_id();

                if ($idretailx) {
                    $sql_user="INSERT INTO usuarios (idretail,login,clave,nombres,correo,admin)
                               SELECT * FROM (SELECT '$idretailx','$username','$clave','$nombre_admin','$email','0') AS tmp
                               WHERE NOT EXISTS (
                                SELECT id FROM usuarios WHERE idretail=(SELECT id FROM retailers WHERE nit='$nit' LIMIT 1)
                               ) LIMIT 1;"; 
                    
                } else {
                    $id_ret="(SELECT id FROM retailers WHERE nit='$nit' LIMIT 1)";
                    $sql_user="UPDATE usuarios SET
                                login='$username'
                                ,clave='$clave'
                                ,nombres='$nombre_admin'
                                ,correo='$email'
                                WHERE idretail=$id_ret
                               "; 
                }
                $query2=mysql_query($sql_user,$connection);

                if(!$query) die(mysql_error());
               }//end for
            }//end if
            header("Location: ../retailers.php");
        }//endif
    }
}  else {
    die("Error, Return Code: " . $_FILES["csv"]["error"] . "<br />"); 
	header("Location: ../retailers.php");
}
?>