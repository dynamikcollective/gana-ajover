<?php
include("../session.php");
ob_clean();
header("Content-Type: text/csv");
header("Content-Transfer-Encoding: UTF-8");
header("Content-Disposition: attachment; filename=report.csv");
header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
header("Pragma: no-cache"); // HTTP 1.0
header("Expires: 0"); // Proxies

$sql = "SELECT r.social, r.nit, r.codigo,
    r.direccion,
    v.cedula, v.nombre, v.email,
    m.producto AS meta,
    p.descripcion AS premio,
    vp.cantidad
FROM vendedores_premios vp
    INNER JOIN vendedores_general v ON ( vp.idvendedor = v.id )
    INNER JOIN premios p ON ( vp.premio = p.id )
    INNER JOIN retailers r ON ( v.idretail = r.id )
    INNER JOIN metas m ON(vp.idmeta=m.id)
ORDER BY r.social, v.nombre";
$query = mysql_query($sql);

$output = fopen("php://output", "w");
fputcsv($output, array('Razon Social', 'NIT', 'Codigo','direccion','Cedula','Nombre','Email','Meta','Premio','Cantidad'), ";");
while ($row = mysql_fetch_assoc($query)) fputcsv($output, $row, ";");
fclose($output);
?>