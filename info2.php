<?php include 'includes/head.php'; ?>
<?php include 'includes/main-header.php'; ?>
<!-- Left side column. contains the logo and sidebar -->
<?php include 'includes/main-menu.php'; ?>


<section class="content-header">
    <h1>
        Administrar parametros generales de Gana Ajover<br/>
        <small>Ingresa la información para definir la mercancía del concurso</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Productos de</li>
    </ol>
</section>


<section>
    <div class="col-md-12">
        <img src="https://upload.wikimedia.org/wikipedia/commons/1/14/Panorama_puy_de_dome_sud.jpg" class="img-responsive">
        <br>
    </div>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <!-- general form elements -->
           

            <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Productos</h3>
                        <p>A continuacion podras ingresar todos los productos del catalogo de Ajover.</p>
                    </div>
            </div>

            <div class="box box-primary">
                <div class="box-header with-border">
                        <form class="form-horizontal">
                          <div class="box-body">
                            <div class="form-group">
                              <label for="inputEmail3" class="col-sm-3 control-label">Producto 1</label>
                              <div class="col-sm-9">
                                <input type="text" class="form-control" id="price1Text" placeholder="Cubierta Max">
                              </div>
                            </div>

                            <div class="form-group">
                              <label for="inputPassword3" class="col-sm-3 control-label">Producto 2</label>
                              <div class="col-sm-9">
                                <input type="text" class="form-control" id="price2Text" placeholder="Tanques Sépticos">
                              </div>
                            </div>
                            
                          </div>
                          <!-- /.box-body -->
                          <div class="col-md-3"></div>

                          <div class="col-md-4">
                              <button type="submit" class="btn btn-info"><i class="fa fa-plus"></i> Agregar Otro Premio</button>
                          </div>

                          <div class="col-md-5">
                              <button type="submit" class="btn btn-info pull-right">Nuevos productos desde CSV</button>
                          </div>
                          <!-- /.box-footer -->
                        </form>

                        <br/><br/>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info pull-right"> Guardar Actualización </button>
                        </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php include 'includes/footer.php'; ?>
