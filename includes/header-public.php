<?php include('session.php');  //control de sesion del usuario
$login_session=isset($_SESSION['nuser'])?$_SESSION['nuser']:$login_session;
?>
<header class="main-header">
    <!-- Logo -->
    <?php
      if ($idretail>0) {
          print '<a href="dashboard-public.php" class="logo">' ;
      } else {
         print ' <a href="index.php" class="logo">';
      } //end if
    ?>
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="assets/logo-main.jpg" alt="" width="100%"></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Gana</b>Ajover</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="dist/img/icon-user-default.png" class="user-image" alt="User Image">
              <span class="hidden-xs">  <?php echo $login_session; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="dist/img/icon-user-default.png" class="img-circle" alt="User Image">

                <p>
                   <?php echo $login_session; ?>, Responsable
                </p>
              </li>
              <!-- Menu Body -->
              
              <!-- Menu Footer...login2.html-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="profile.php" class="btn btn-default btn-flat">Perfil</a>
                </div>
                <div class="pull-right">
                  <a href="./logout.php" class="btn btn-default btn-flat">Salir</a>
                </div>
              </li>
            </ul>
          </li>
          
        </ul>
      </div>
    </nav>
  </header>