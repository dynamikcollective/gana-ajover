<?php
if($_SESSION['admin']!=0) {
  die("<h4>Perfil no tiene permiso para ingresar en esa &aacute;rea <a href=\"login1.php\">[inicie sesion]</a> nuevamente</h4>");
}
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/icon-user-default.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $login_session; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">NAVEGACI&Oacute;N</li>
        <li class="active treeview">
          <a href="dashboard-public.php">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="new-seller.php">
            <i class="fa fa-user-plus"></i> <span>Agregar Vendedor</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">