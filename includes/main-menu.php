<?php
include('session.php');  //control de sesion del usuario
if($_SESSION['admin']!=1) {
  die("<h4>Verifique las credenciales del usuario o <a href=\"login1.php\">[inicie sesion]</a> nuevamente</h4>");
}
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/icon-user-default.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $login_session; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">NAVEGACI&Oacute;N</li>
        <li class="active treeview">
          <a href="index.php">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="active treeview">
          <a href="info.php">
            <i class="fa fa-info-circle "></i> <span>Informaci&oacute;n del concurso</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Parametros de campa&ntilde;a</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="goals.php"><i class="fa fa-circle-o"></i>Metas</a></li>
            <li><a href="premios.php"><i class="fa fa-circle-o"></i>Premios</a></li>
            <li><a href="products.php"><i class="fa fa-circle-o"></i>Productos</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="retailers.php">
            <i class="fa fa-suitcase"></i> <span>Administracion de retailers</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="retailers.php"><i class="fa fa-circle-o"></i>vista de retailers</a></li>
            <li><a href="new-retailer.php"><i class="fa fa-circle-o"></i>A&ntilde;adir retailer</a></li>
            <li><a href="prizes-report.php"><i class="fa fa-circle-o"></i>Premios solicitados</a></li>
            <!-- <li><a href="edit-retailer.php"><i class="fa fa-circle-o"></i>Editaretailer</a></li> -->
            <!-- <li><a href="sellers.php"><i class="fa fa-circle-o"></i>Vendedores</a></li> -->
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">