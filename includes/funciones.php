<?php
//*** Funcion de envio de correo desde Retailer
function enviacorreo($destino, $mensaje) {
     $subject = "[GanaAjover] Solicitud de premios desde retailer";
     $headers   = array();
     $headers[] = "MIME-Version: 1.0";
     $headers[] = "Content-type: text/html; charset=iso-8859-1";
     $headers[] = "From: Gana Ajover <noresponder@ajover.com>";
     $headers[] = "X-Mailer: PHP/".phpversion();

     mail($destino, $subject, $mensaje, implode("\r\n", $headers));
}//end function

//*** Funcion de envio de correo
function alertar($email, $idvendedor) {
  include "conecta.php";
  $sql = "SELECT vg.premio,vg.email,
               CONCAT(vg.nombre, ' ',vg.apellido) AS vendedor,
               r.nit, r.comercial
          FROM vendedores_general vg
               INNER JOIN retailers r ON(vg.idretail=r.id)
          WHERE vg.id=$idvendedor";
  $consulta = @mysql_query($sql);

  if ($consulta) {
     $row = mysql_fetch_assoc($consulta);
     $premio = $row['premio'];
     $vendedor =  $row['vendedor'];
     $nit = $row['nit'];
     $retail = $row['comercial'];
     $email_vendedor =  $row['email'];

     $subject = "[GanaAjover] Notificacion de Meta Cumplida";
     $headers   = array();
     $headers[] = "MIME-Version: 1.0";
     $headers[] = "Content-type: text/html; charset=iso-8859-1";
     $headers[] = "From: Gana Ajover <noresponder@ajover.com>";
     $headers[] = "X-Mailer: PHP/".phpversion();

     $mensaje = "<b>Retailer</b>: $nit / $retail<br />".
              "<b>Vendedor</b>: $idvendedor / $vendedor<br />".
              "<b>Email Vendedor</b>: $email_vendedor<br />".
              "<b>Premio Seleccionado: <font color='red'>$premio</font></b><br />".
              "<br /><p><small>Este correo ha sido enviado automaticamente desde el sistema GanaAjover desde una cuenta no monitoreada, favor no responder a este mensaje.".
              "<br />si cree que ha recibido este mensaje por error haga caso omiso del contenido y notifiquelo inmediatamente a la cuenta notificar@ajover.com.</small></p>";

     mail($email, $subject, $mensaje, implode("\r\n", $headers));
  } //end if
  
}//end function

//*** Funcion de Verificacion de Metas por Vendedor
function revisa_meta($idvendedor) {
  // ---> anular uso de esta funcion por cambios en la mec�nica de premios
  return
  // <---
  include "conecta.php";
  $sql = "SELECT idretail FROM vendedores_general WHERE id=$idvendedor";
  $consulta =  mysql_query($sql);
  if ($row = mysql_fetch_assoc($consulta)) {
    $idretail=$row['idretail'];
  } else {
    return;
  }//end if

  //*** Obtenemos las metas
  $sql = "SELECT producto, cantidad, unidad FROM metas";
  $consulta = mysql_query($sql);
  $metas = array();
  $i=0;
  while ($row = mysql_fetch_assoc($consulta)) {
     $metas[$i]['producto'] = $row['producto'];
     $metas[$i]['cantidad'] = $row['cantidad'];
     $metas[$i]['unidad'] = $row['unidad'];
     $i++;
  }//endwhile
  
  //*** Obtenemos las cantidades maximas permitidas
  $sql = "SELECT m.producto, r.cantidad, m.unidad
          FROM retailers_max r
               INNER JOIN metas m ON(r.idmeta=m.id)
          WHERE r.idretail=$idretail";
  $consulta = mysql_query($sql);
  $max = array();
  $i=0;
  while ($row = mysql_fetch_assoc($consulta)) {
     $max[$i]['producto'] = $row['producto'];
     $max[$i]['cantidad'] = $row['cantidad'];
     $max[$i]['unidad'] = $row['unidad'];
     $i++;
  }//endwhile

  //*** Obtenemos las cantidades conseguidas por el Vendedor
  $sql = "SELECT m.producto, v.cantidad, m.unidad
          FROM vendedores_detalles v
               INNER JOIN metas m ON(v.idmeta=m.id)
          WHERE v.idvendedor=$idvendedor";
  $consulta = mysql_query($sql);
  $ventas = array();
  $i = 0;
  $cumplida = 0;
  $maximas = 0;
  while ($row = mysql_fetch_assoc($consulta)) {
     $producto = $row['producto'];
     $cantidad = $row['cantidad'];
     $unidad = $row['unidad'];
     //*** Recorremos las metas
     for($i=0; $i<count($metas); $i++) {
        if($producto == $metas[$i]['producto'] && $unidad == $metas[$i]['unidad'] && $cantidad >= $metas[$i]['cantidad'])
          $cumplida++;
     }//end for
     
     //*** Recorremos los maximos permitidos
     for($i=0; $i<count($max); $i++) {
        if($producto == $max[$i]['producto'] && $unidad == $max[$i]['unidad'] && $cantidad <= $max[$i]['cantidad'])
          $maximas++;
     }//end for
  }//endwhile

  //*** Evaluamos las metas y validamos las cantidades maximas permitidas para enviar correo
  if((count($metas) == $cumplida) && ($cumplida == $maximas)) {
     $consulta = mysql_query("SELECT email FROM usuarios WHERE admin=1");
     while( $row = mysql_fetch_assoc($consulta)) {
       alertar($row['correo'], $idvendedor);
     }//end while
  /* } else {
     echo "Metas: ".count($metas)."<br />Cumplidas: $cumplida<br />Maximos Autorizados: $maximas";
  */
  }//end if
  
} //end function

//alertar("andyjnet@gmail.com", 38);
//revisa_meta(38);

?>