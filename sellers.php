<?php
include('session.php');
include 'includes/head.php';
include 'includes/main-header.php';
include 'includes/main-menu.php';

// *** Eliminar Registro
if(isset($_GET['eid']) && isset($_GET['hash'])) {
  $eid = $_GET['eid'];
  $aud = $_GET['hash'];
  if ($aud == md5($eid.'ajbcrjbc')) {
     $res = mysql_query("delete from retailers where id=$eid");
     $res = mysql_query("delete from usuarios where idretail=$eid");
  }//endif
}//endif

// *** Buscar datos para la tabla de Vendedores
$sql = "SELECT r.id, r.social, r.nombre_admin
     FROM retailers r
     ORDER BY r.social";
$query = mysql_query($sql);
$linea = 1;
?>

<script type="text/javascript">
  function eliminar(social, eid, hash) {
    var resp = confirm("Confirme que desea eliminar Retail ("+social+")?");
    if (resp == true) {
       window.location.href='?eid='+eid+'&hash='+hash;
    }
  }
</script>

<section class="content-header">
    <h1>
        Administrar Vendedores<br/><br/>
        <small>
         Complete el siguiente formulario para actualizar la informacion de cada concurso de GanaAjover.
        </small>
    </h1>

    <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Retailers</li>
    </ol>
</section>

<br/><br/>
<section class="sellers-table">
    <div class="col-xs-12">
        <div class="table-responsive">
        <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
            <thead>
                <tr role="row">
                    <th class="text-center sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending">Retail</th>
                    <th class="text-center sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column descending" aria-sort="ascending">Nombre</th>
                    <th class="text-center sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column descending" aria-sort="ascending">Apellido</th>
                    <th class="text-center sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column descending" aria-sort="ascending">Correo</th>
                    <th class="text-center sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Meta 1</th>
                    <th class="text-center sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">Meta 2</th>
                    <th class="text-center sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">Meta 3</th>
                    <th class="text-center sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">Meta 4</th>
                    <th class="text-center sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">Acciones</th>
                </tr>
            </thead>
            <tbody>
                <tr role="row" class="odd">
                    <td class="text-center">001</td>
                    <td class="text-center">Pepe</td>
                    <td class="text-center">Perez</td>
                    <td class="text-center">rgomez@ferremax.com</td>
                    <td class="text-center">23 de 250</td>
                    <td class="text-center">34 de 35</td>
                    <td class="text-center">20 de 20</td>
                    <td class="text-center">9 de 30</td>
                    <td class="text-center" style="text-align:center" align="center">
                        <div class ="btn-group col-xs-12">
                            <button type="submit" class="btn btn-xs btn-primary">Editar</button>
                            <button type="submit" class="btn btn-xs btn-secondary">Eliminar</button>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>


    <div id="row" class="row">
    	<div class ="col-md-3 col-md-offset-6">
            <button type="submit" class="btn btn-primary pull-right">Agregar nuevo vendedor</button>
        </div>
        <div class ="col-md-3">
            <button type="submit" class="btn btn-primary">Nuevos vendedores desde CSV</button>
        </div>
    </div>
</section>

<hr>







<?php include 'includes/footer.php'; ?>

